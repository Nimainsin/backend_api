// eslint-disable-next-line no-unused-vars
const QuestionBankHindi = require("./questionBankHindiModel");

exports.insertQuestion = question => QuestionBankHindi.create(question)

exports.insertBulkQuestion = questions => QuestionBankHindi.insertMany(questions)

exports.editQuestion = (question, _id) => QuestionBankHindi.findByIdAndUpdate({ _id }, question, { new: true })

exports.getAllQuestions = () => QuestionBankHindi.find().populate("questionCategory")

exports.getQuestion = _id => QuestionBankHindi.findOne({ _id }).populate("questionCategory")

exports.deleteQuestion = _id => QuestionBankHindi.remove({ _id })