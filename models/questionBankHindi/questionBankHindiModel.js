// Create your State model's schema here and export it.

const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const questionBankHindiSchema = new mongoose.Schema(
    {
        questionTitle: { type: String, required: true, unique: true },
        option1: { type: String, required: true },
        option2: { type: String, required: true },
        option3: { type: String },
        option4: { type: String },
        answer: { type: String, required: true },
        questionCategory: { type: Schema.Types.ObjectId, ref: "QuestionCategory" },
        questionCoinPoints: { type: Number, required: true },
        multipleChoice: { type: Boolean, default: false, required: true },
    },
    {
        timestamps: true,
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                return ret;
            },
        },
    }
);

const QuestionBankHindi = mongoose.model("QuestionBankHindi", questionBankHindiSchema);
QuestionBankHindi.createIndexes();
module.exports = QuestionBankHindi;
