const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const rolePermission = new mongoose.Schema(
    {
        permission: { type: String, required: true, unique: true },
    },
    {
        timestamps: true,

    }
);

const RolePermission = mongoose.model("RolePermission", rolePermission);
RolePermission.createIndexes()
module.exports = RolePermission;