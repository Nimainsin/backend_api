// eslint-disable-next-line no-unused-vars
const Vacancy = require("./vacancyModel");

exports.insertVacancy = (vacancy) => Vacancy.create(vacancy)

exports.editVacancy = (vacancy, _id) => Vacancy.findOneAndUpdate({ _id }, vacancy, { new: true })

exports.getVacancy = (_id) => Vacancy.findOne({ _id })

exports.getAllVacancy = () => Vacancy.find()

exports.deleteVacancy = (_id) => Vacancy.remove({ _id })