
const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const vacancySchema = new mongoose.Schema({
    role: { type: String, required: true },
    location: { type: String, required: true },
    skills: { type: String, required: true },
    description: { type: String, required: true },
    positionCount: { type: String, required: true },
    seniorityLevel: { type: String, required: true },
}, { timestamps: true })


const Vacancy = mongoose.model("Vacancy", vacancySchema)

module.exports = Vacancy