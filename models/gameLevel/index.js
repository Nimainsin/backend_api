// eslint-disable-next-line no-unused-vars
const GameLevel = require("./gameLevelModel");

exports.insertGameLevel = (gameLevel) => GameLevel.create(gameLevel);

exports.editGameLevel = (gameLevel, id) =>
    GameLevel.findByIdAndUpdate({ _id: id }, gameLevel, { new: true });

exports.findGameLevel = (id) => GameLevel.findOne({ _id: id }).populate({
    path: "contest",
    populate: {
        path: "group",
        populate: [
            {
                path: "village",
                populate: {
                    path: "block",
                    populate: {
                        path: "district",
                        populate: {
                            path: "state",
                        },
                    },
                },
            },
            {
                path: "user",
                populate: [
                    {
                        path: "role",
                        populate: {
                            path: "permissions"
                        }
                    },
                    { path: "energy" },
                ],
            },
        ],
    },
});

exports.findAllGameLevels = () =>
    GameLevel.find().populate({
        path: "contest",
        populate: {
            path: "group",
            populate: [
                {
                    path: "village",
                    populate: {
                        path: "block",
                        populate: {
                            path: "district",
                            populate: {
                                path: "state",
                            },
                        },
                    },
                },
                {
                    path: "user",
                    populate: [
                        {
                            path: "role",
                            populate: {
                                path: "permissions"
                            }
                        },
                        { path: "energy" },
                    ],
                },
            ],
        },
    });

exports.removeGameLevel = (id) => GameLevel.remove({ _id: id });
