
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const gameLevelSchema = new mongoose.Schema({
    name: { type: String, required: true, },
    gameLevel: { type: Number, enum: [1, 2, 3, 4] },
    obstacle: [{ type: Schema.Types.ObjectId, ref: "Obstacle" }],
    reward: [{ type: Schema.Types.ObjectId, ref: "Reward" }],
    memoryGame: [{ type: Schema.Types.ObjectId, ref: "MemoryGame" }],
    contest: [{ type: Schema.Types.ObjectId, ref: "GameContest" }],
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },

})

const Level = mongoose.model("Level", gameLevelSchema)
Level.createIndexes()
module.exports = Level