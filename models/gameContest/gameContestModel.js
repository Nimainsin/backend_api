const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const gameContestSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String, default: "" },
    image: { type: String, default: "" },
    maximumGroup: { type: Number, default: 2, enum: [2, 4, 8, 16] },
    winningAmount: { type: String, default: 0 },
    contestCode: { type: Number, default: null },
    contestStartDate: { type: Date, default: null },
    contestStartTime: { type: String },
    contestEndDate: { type: Date, default: null },
    contestEndTime: { type: String },
    status: { type: String, default: "active", enum: ["active", "inactive"] },
    group: [{ type: Schema.Types.ObjectId, ref: "Group" }],
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const GameContest = mongoose.model("GameContest", gameContestSchema);
GameContest.createIndexes();
module.exports = GameContest;
