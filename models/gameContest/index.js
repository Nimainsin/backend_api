// eslint-disable-next-line no-unused-vars
const GameContest = require("./gameContestModel");

exports.insertContest = (Contest) => GameContest.create(Contest);

exports.editContest = (Contest, id) =>
    GameContest.findByIdAndUpdate({ _id: id }, Contest, { new: true });

exports.findContest = (id) =>
    GameContest.findOne({ _id: id }).populate({
        path: "group",
        populate: [{
            path: "user",
            populate: [{
                path: "role",
                populate: {
                    path: "permissions",
                },
            },
            {
                path: "energy",
            },
            ],
        },
        {
            path: "village",
            populate: {
                path: "block",
                populate: {
                    path: "district",
                    populate: {
                        path: "state",
                    },
                },
            },
        },
        ],
    });

exports.findAllContests = () =>
    GameContest.find().populate({
        path: "group",
        populate: [{
            path: "user",
            populate: [{
                path: "role",
                populate: {
                    path: "permissions",
                },
            },
            {
                path: "energy",
            },
            ],
        },
        {
            path: "village",
            populate: {
                path: "block",
                populate: {
                    path: "district",
                    populate: {
                        path: "state",
                    },
                },
            },
        },
        ],
    });

exports.removeContest = (id) => GameContest.remove({ _id: id });

exports.getLiveContestCount = () => GameContest.count({ status: "active" })