// Create your Village model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const districtSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    state: { type: Schema.Types.ObjectId, ref: "State" },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const District = mongoose.model("District", districtSchema);
District.createIndexes();
module.exports = District;
