// eslint-disable-next-line no-unused-vars
const District = require("./districtModel");

exports.getAllDistricts = () => District.find().populate("state")

exports.getDistrict = (_id) => District.findOne({ _id }).populate('state')

exports.createDistrict = (district) => District.create(district)

exports.editDistrict = (district, _id) => District.findByIdAndUpdate({ _id }, district, { new: true })

exports.deleteDistrict = (_id) => District.remove({ _id })

exports.getDistrictByStateId = (stateId) => District.find({ state: stateId })