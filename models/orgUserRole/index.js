// eslint-disable-next-line no-unused-vars
const OrgUserRole = require("./orgUserRoleModel");

exports.insertRole = (role) => OrgUserRole.create(role)

exports.getAllRoles = () => OrgUserRole.find({ archive: false }).populate("module")

exports.getRole = (_id) => OrgUserRole.findOne({ _id, archive: false }).populate("module")

exports.editRole = (role, _id) => OrgUserRole.findOneAndUpdate({ _id }, role, { new: true })

exports.deleteRole = (_id) => OrgUserRole.findOneAndUpdate({ _id }, { archive: true }, { new: true })