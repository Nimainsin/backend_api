const mongoose = require('kvell-db-plugin-mongoose').dbLib
const Schema = mongoose.Schema
const orgUserRoleSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    module: [{ type: Schema.Types.ObjectId, ref: 'OrgModule2' }],
    archive: { type: Boolean, default: false, required: true },
  },
  {
    timestamps: true,
  }
)

const OrgUserRole = mongoose.model('OrgUserRole', orgUserRoleSchema)
OrgUserRole.createIndexes()
module.exports = OrgUserRole
