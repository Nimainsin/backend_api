// eslint-disable-next-line no-unused-vars
const Village = require("./villageModel");

exports.insertVillage = (village) => Village.create(village);

exports.editVillage = (village, id) =>
  Village.findByIdAndUpdate({ _id: id }, village, { new: true });

exports.findVillage = (id) =>
  Village.findOne({ _id: id }).populate({
    path: "block",
    populate: {
      path: "district",
      populate: {
        path: "state",
      },
    },
  });

exports.findAllVillages = () =>
  Village.find().sort('name').populate({
    path: "block",
    populate: {
      path: "district",
      populate: {
        path: "state",
      },
    },
  })

exports.findAllVillagesByBlock = (blockId) =>
  Village.find({ block: blockId }).sort('name').populate({
    path: "block",
    populate: {
      path: "district",
      populate: {
        path: "state",
      },
    },
  })

exports.removeVillage = (id) => Village.remove({ _id: id });
