
// Create your Village model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const villageSchema = new mongoose.Schema({
    name: { type: String, required: true },
    block: { type: Schema.Types.ObjectId, ref: "Block" },
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
})

const Village = mongoose.model("Village", villageSchema)
Village.createIndexes()
module.exports = Village