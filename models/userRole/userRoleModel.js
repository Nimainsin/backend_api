const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema
const userRoleSchema = new mongoose.Schema(
    {
        role: { type: String, required: true, unique: true, enum: ["super admin", "admin", "user"] },
        permissions: [{ type: Schema.Types.ObjectId, ref: "RolePermission" }]
    },
    {
        timestamps: true,
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                return ret;
            },
        },
    }
);

const UserRole = mongoose.model("UserRole", userRoleSchema);
UserRole.createIndexes()
module.exports = UserRole;
