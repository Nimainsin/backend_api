// eslint-disable-next-line no-unused-vars
const UserRole = require("./userRoleModel");

exports.getUserRole = (role) => UserRole.findOne({ role })