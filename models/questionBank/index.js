// eslint-disable-next-line no-unused-vars
const QuestionBank = require("./questionBankModel");

exports.insertQuestion = question => QuestionBank.create(question)
exports.insertBulkQuestion = questions => QuestionBank.insertMany(questions)

exports.editQuestion = (question, _id) => QuestionBank.findByIdAndUpdate({ _id }, question, { new: true })

exports.getAllQuestions = () => QuestionBank.find().populate("questionCategory")

exports.getQuestion = _id => QuestionBank.findOne({ _id }).populate("questionCategory")

exports.deleteQuestion = _id => QuestionBank.remove({ _id })