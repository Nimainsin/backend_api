// eslint-disable-next-line no-unused-vars
const Rank = require("./rankModel");

exports.insertRank = (rank) => Rank.create(rank)

exports.editRank = (rank, id) => Rank.findByIdAndUpdate({ _id: id }, rank, { new: true })

exports.findRank = (id) => Rank.findOne({ _id: id }).populate(["user", "group"])

exports.findAllRanks = (id) => Rank.find({ contest: [id] })
    .populate([{
        path: "user"
    }
        , {
        path: "contest",
        populate: {
            path: "group",
            populate: {
                path: "user"
            }
        }
    },
    ])

exports.findAllRank = () => Rank.find()

exports.checkUniqueRank = (user, contest) => Rank.findOne({ user, contest })

exports.removeRank = (id) => Rank.remove({ _id: id })