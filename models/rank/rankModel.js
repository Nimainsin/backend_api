
// Create your Rank model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib
const Schema = mongoose.Schema
const rankSchema = new mongoose.Schema({
    contest: [{ type: Schema.Types.ObjectId, ref: "GameContest" }],
    user: [{ type: Schema.Types.ObjectId, ref: "User" }],
    position: { type: String },
    levelname: { type: String },
    energy: { type: String },
    points: { type: String },

}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret,) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
})

const Rank = mongoose.model("Rank", rankSchema)
Rank.createIndexes()
module.exports = Rank