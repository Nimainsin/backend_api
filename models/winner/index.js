// eslint-disable-next-line no-unused-vars
const Winner = require("./winnerModel");

exports.insertWinner = (winner) => Winner.create(winner)

exports.editWinner = (winner, id) => Winner.findByIdAndUpdate({ _id: id }, winner, { new: true })

exports.findWinner = (id) => Winner.findOne({ contenstId: id })

exports.findAllWinners = (id) => Winner.find({ contestId: id })

exports.removeWinner = (id) => Winner.remove({ _id: id })