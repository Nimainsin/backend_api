
// Create your State model's schema here and export it.

const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const winnerSchema = new mongoose.Schema({
    contestId: { type: String, required: true },
    userId: { type: String, required: true },

}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret,) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
})

const Winner = mongoose.model("Winner", winnerSchema)
Winner.createIndexes()
module.exports = Winner