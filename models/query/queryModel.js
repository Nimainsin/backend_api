
const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const querySchema = new mongoose.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    company: { type: String, required: true },
    query: { type: String, required: true },
}, { timestamps: true })


const Query = mongoose.model("Query", querySchema)

module.exports = Query