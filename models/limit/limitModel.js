
const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const limitSchema = new mongoose.Schema({
    limit: { type: Number, required: true },
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },

})

const Limit = mongoose.model("Limit", limitSchema)
Limit.createIndexes()
module.exports = Limit