// eslint-disable-next-line no-unused-vars
const Limit = require("./limitModel");

exports.createLimit = (limit) => Limit.create(limit)

exports.editLimit = (limit, id) => Limit.findByIdAndUpdate(id, limit, { new: true })

exports.getAllLimits = () => Limit.find()

exports.getLimit = (_id) => Limit.findOne({ _id })

exports.deleteLimit = (_id) => Limit.deleteOne({ _id })