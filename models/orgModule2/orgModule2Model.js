const mongoose = require('kvell-db-plugin-mongoose').dbLib
const Schema = mongoose.Schema
const moduleSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    type: {
      type: String,
      required: true,
      enum: ['item', 'collapse', 'groupHeader'],
      default: 'item',
    },
    icon: { type: String, required: true },
    navLink: { type: String, required: true },
    groupTitle: { type: String, default: '' },
    parentId: { type: String, default: '' }, //[{ type: Schema.Types.ObjectId, ref: 'OrgModule2' }],
    permissions: { type: Array, default: [] },
    roles: [{ type: Schema.Types.ObjectId, ref: 'OrgUserRole' }],
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id
        delete ret._id
        delete ret.__v
        return ret
      },
    },
  }
)

const OrgModule2 = mongoose.model('OrgModule2', moduleSchema)
OrgModule2.createIndexes()
module.exports = OrgModule2
