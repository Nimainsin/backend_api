// eslint-disable-next-line no-unused-vars
const OrgModule2 = require('./orgModule2Model')

exports.insertOrgModule = orgModule => OrgModule2.create(orgModule)

exports.getAllOrgModules = () => OrgModule2.find().populate(['roles'])

exports.getOrgModule = _id => OrgModule2.findOne({ _id }).populate(['roles'])

exports.editOrgModule = (orgModule, _id) =>
  OrgModule2.findOneAndUpdate({ _id }, orgModule, { new: true })

exports.deleteOrgModule = _id => OrgModule2.remove({ _id })
