// eslint-disable-next-line no-unused-vars
const Chat = require("./chatModel");

exports.getAllChats = (cond) => Chat.find(cond).populate(["contest", "group", "user"])

exports.getChat = (_id) => Chat.findOne({ _id })

exports.createChat = (chat) => Chat.create(chat)

exports.createEmptyChat = (chat) => Chat.create(chat)

exports.editChat = (chat, _id) => Chat.findByIdAndUpdate({ _id }, chat, { new: true })

exports.deleteChat = (_id) => Chat.remove({ _id })