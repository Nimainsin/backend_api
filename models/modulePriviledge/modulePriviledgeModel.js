// Create your State model's schema here and export it.

const mongoose = require('kvell-db-plugin-mongoose').dbLib
const Schema = mongoose.Schema
const modulePriviledgeSchema = new mongoose.Schema(
  {
    moduleId: { type: Schema.Types.ObjectId, ref: 'OrgModule2' },
    read: { type: Boolean, default: false },
    write: { type: Boolean, default: false },
    create: { type: Boolean, default: false },
    delete: { type: Boolean, default: false },
    permissionAssignedBy: { type: String },
    permissionAssignTo: { type: String, required: true },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id
        delete ret._id
        delete ret.__v
        return ret
      },
    },
  }
)

const ModulePriviledge = mongoose.model('ModulePriviledge', modulePriviledgeSchema)
ModulePriviledge.createIndexes()
module.exports = ModulePriviledge
