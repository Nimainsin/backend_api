// eslint-disable-next-line no-unused-vars
const ModulePriviledge = require("./modulePriviledgeModel");

exports.createModulePriviledgeInBulk = (modulePriviledges) =>
  ModulePriviledge.insertMany(modulePriviledges);

exports.getAllModulePriviledges = () => ModulePriviledge.find().populate("module")

exports.getModulePriviledge = async (_id) =>
  await ModulePriviledge.find({ permissionAssignTo: _id }).populate("module")

exports.createModulePriviledge = (modulePriviledge) =>
  ModulePriviledge.create(modulePriviledge);

exports.editModulePriviledge = (modulePriviledge, _id) =>
  ModulePriviledge.findByIdAndUpdate({ _id }, modulePriviledge, { new: true });

exports.deleteModulePriviledge = (_id) => ModulePriviledge.deleteOne({ _id });

exports.deleteModulePriviledgeInBulk = (_id) =>
  ModulePriviledge.deleteMany({ permissionAssignTo: _id });
