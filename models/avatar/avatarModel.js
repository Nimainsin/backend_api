// Create your Village model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const avatarSchema = new mongoose.Schema(
    {
        name: { type: String, required: true, unique: true },
        avatarId: { type: String, required: true, unique: true, default: 0 }
    },
    {
        timestamps: true,
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                return ret;
            },
        },
    }
);

const Avatar = mongoose.model("Avatar", avatarSchema);
Avatar.createIndexes();
module.exports = Avatar;
