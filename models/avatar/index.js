// eslint-disable-next-line no-unused-vars
const Avatar = require("./avatarModel");

exports.getAllAvatars = () => Avatar.find()

exports.getAvatar = (_id) => Avatar.findOne({ _id })

exports.createAvatar = (avatar) => Avatar.create(avatar)

exports.avatarCount = () => Avatar.count()

exports.editAvatar = (avatar, _id) => Avatar.findByIdAndUpdate({ _id }, avatar, { new: true })

exports.deleteAvatar = (_id) => Avatar.remove({ _id })