const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const userSchema = new mongoose.Schema(
  {
    mobile: { type: Number, required: true, unique: true },
    email: { type: String, default: "" },
    name: { type: String, default: "" },
    age: { type: Number, default: "" },
    gender: { type: String, enum: ["male", "female", "trans gender"] },
    userPoints: { type: Number, default: 0 },
    imageUrl: { type: String, default: "" },
    otpToken: { type: Number, default: 1234 },
    isMobileVerified: { type: Boolean, default: false },
    countryCode: { type: String, default: "" },
    status: { type: String, enum: ["active", "inactive"], default: "inactive" },
    role: [{ type: Schema.Types.ObjectId, ref: "UserRole" }],
    energy: [{ type: Schema.Types.ObjectId, ref: "Energy" }],
    village: { type: Schema.Types.ObjectId, ref: "Village" },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const User = mongoose.model("User", userSchema);
User.createIndexes();
module.exports = User;
