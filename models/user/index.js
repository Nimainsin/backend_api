// eslint-disable-next-line no-unused-vars
const User = require("./userModel");

exports.insertUser = (user) => User.create(user);

exports.editUser = (user, _id) =>
  User.findByIdAndUpdate(
    {
      _id: _id,
    },
    user,
    {
      new: true,
    }
  );

exports.findUser = (id) =>
  User.findOne({
    _id: id,
  }).populate([
    {
      path: "role",
      populate: {
        path: "permissions",
      },
    },
    {
      path: "energy",
    },
    {
      path: "village",
      populate: {
        path: "block",
        populate: {
          path: "district",
          populate: {
            path: "state",
          },
        },
      },
    },
  ]);

exports.findUserByFilter = async (filter) => {
  return await User.findOne(filter).populate([
    {
      path: "role",
      populate: {
        path: "permissions",
      },
    },
    {
      path: "energy",
    },
    {
      path: "village",
      populate: {
        path: "block",
        populate: {
          path: "district",
          populate: {
            path: "state",
          },
        },
      },
    },
  ]);
};
exports.findUserByMobile = (mobile) =>
  User.findOne({
    mobile,
  });

exports.findAllUsers = () =>
  User.find().populate([
    {
      path: "role",
      populate: {
        path: "permissions",
      },
    },
    {
      path: "energy",
    },
    {
      path: "village",
      populate: {
        path: "block",
        populate: {
          path: "district",
          populate: {
            path: "state",
          },
        },
      },
    },
  ]);

exports.removeUser = (id) =>
  User.remove({
    _id: id,
  });

exports.findUserByVillage = (id) =>
  User.find({
    village: [id],
  }).populate([
    {
      path: "role",
      populate: {
        path: "permissions",
      },
    },
    {
      path: "energy",
    },
    {
      path: "village",
      populate: {
        path: "block",
        populate: {
          path: "district",
          populate: {
            path: "state",
          },
        },
      },
    },
  ]);

exports.getUserCount = () => User.count()
