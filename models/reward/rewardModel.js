// Create your State model's schema here and export it.

const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const RewardSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    image: { type: String },
    point: { type: String, required: true },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const reward = mongoose.model("Reward", RewardSchema);
reward.createIndexes();
module.exports = reward;
