// eslint-disable-next-line no-unused-vars
const Reward = require("./rewardModel");

exports.insertReward = reward => Reward.create(reward)

exports.editReward = (reward, _id) => Reward.findByIdAndUpdate({ _id }, reward, { new: true })

exports.getAllRewards = () => Reward.find()

exports.getReward = _id => Reward.findOne({ _id })

exports.deleteReward = _id => Reward.remove({ _id })