// eslint-disable-next-line no-unused-vars
const Group = require("./groupModel");

exports.insertGroup = (group) => Group.create(group);

exports.editGroup = (group, id) => Group.findByIdAndUpdate({ _id: id }, group, { new: true });

exports.findGroup = (id) =>
    Group.findOne({ _id: id })
        .populate({
            path: "user",
            populate: [
                {
                    path: "role",
                    populate: {
                        path: "permissions",
                    },
                },
                { path: "energy" },
            ],
        })
        .populate({
            path: "village",
            populate: {
                path: "block",
                populate: {
                    path: "district",
                    populate: {
                        path: "state",
                    },
                },
            },
        })
        .populate("energy");

exports.findAllGroups = () =>
    Group.find()
        .populate({
            path: "user",
            populate: [
                {
                    path: "role",
                    populate: {
                        path: "permissions",
                    },
                },
                { path: "energy" },
            ],
        })
        .populate({
            path: "village",
            populate: {
                path: "block",
                populate: {
                    path: "district",
                    populate: {
                        path: "state",
                    },
                },
            },
        });

exports.removeGroup = (id) => Group.remove({ _id: id });

exports.findGroupByName = (name) => Group.findOne({ name })

exports.getGroupCount = () => Group.count()
