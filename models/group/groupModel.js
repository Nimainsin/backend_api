const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const groupSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    image: { type: String, default: null },
    maxOccupancy: { type: Number, default: null },
    groupsPoints: { type: Number, default: null },
    village: { type: Schema.Types.ObjectId, ref: "Village" },
    winnerPosition: {
      type: String,
      enum: ["1st", "2nd", "3rd", "4th"],
    },
    user: [{ type: Schema.Types.ObjectId, ref: "User" }],
    color: { type: String, default: "" }
    // disease: [{ type: Schema.Types.ObjectId, ref: "Disease" }]
  },
  {
    timestamps: true,
    toObject: {
      transform: function (_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const Group = mongoose.model("Group", groupSchema);
Group.createIndexes();
module.exports = Group;
