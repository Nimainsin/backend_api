const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const answerSchema = new mongoose.Schema(
  {
    question: [{ type: Schema.Types.ObjectId, ref: "QuestionBank" }],
    answer: { type: [String] },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const Answer = mongoose.model("Answer", answerSchema);
Answer.createIndexes();
module.exports = Answer;
