// eslint-disable-next-line no-unused-vars
const Answer = require("./answerModel");

exports.insertAnswer = (answer) => Answer.create(answer);

exports.editAnswer = (answer, question) =>
  Answer.findOneAndUpdate({ question }, answer, { new: true });

exports.findAnswer = (id) => Answer.findOne({ _id: id });

exports.findAllAnswers = () =>
  Answer.find().populate({
    path: "question",
    populate: {
      path: "questionCategory",
    },
  });

exports.removeAnswer = (cond) => Answer.remove(cond);

exports.checkAnswer = (query) => Answer.findOne(query);
