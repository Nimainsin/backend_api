// Create your Village model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const blockSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    district: { type: Schema.Types.ObjectId, ref: "District" },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const Block = mongoose.model("Block", blockSchema);
Block.createIndexes();
module.exports = Block;
