// eslint-disable-next-line no-unused-vars
const Block = require("./blockModel");

exports.getAllBlocks = () => Block.find().populate({
    path: "district",
    populate: {
        path: "state"
    }
})

exports.getAllBlocksByDistrict = (districtId) => Block.find({ district: districtId }).populate({
    path: "district",
    populate: {
        path: "state"
    }
})

exports.getBlock = (_id) => Block.findOne({ _id }).populate({
    path: "district",
    populate: {
        path: "state"
    }
})

exports.createBlock = (block) => Block.create(block)

exports.editBlock = (block, _id) => Block.findByIdAndUpdate({ _id }, block, { new: true })

exports.deleteBlock = (_id) => Block.remove({ _id })