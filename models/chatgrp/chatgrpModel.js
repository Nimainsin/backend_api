
// Create your Chatgrp model's schema here and export it.



// Create your State model's schema here and export it.
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema
const chatSchema = new mongoose.Schema(
    {
        contest: [{ type: Schema.Types.ObjectId, ref: "GameContest" }],
        group: [{ type: Schema.Types.ObjectId, ref: "Group" }],
        user: [{ type: Schema.Types.ObjectId, ref: "User" }],
       
    },
    {
        timestamps: true,
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                return ret;
            },
        },
    }
);

const Chatgrp = mongoose.model("Chatgrp", chatSchema);

module.exports = Chatgrp;
