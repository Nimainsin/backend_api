// eslint-disable-next-line no-unused-vars
const Video = require("./videoModel");

exports.insertVideo = (video) => Video.create(video)

exports.editVideo = (video, id) => Video.findByIdAndUpdate({ _id: id }, video, { new: true })

exports.findVideo = (id) => Video.findOne({ _id: id })

exports.findAllVideos = () => Video.find()

exports.removeVideo = (id) => Video.remove({ _id: id })