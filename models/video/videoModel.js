const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const videoSchema = new mongoose.Schema({
    name: { type: String, default: "" },
    description: { type: String, default: "" },
    urlLink: { type: String, default: "" },
    fileType: { type: String, enum: ["webm", "flv", "wmv", "amv", "mp4", "m4p", "3gp", "gif"], default: null },
    previewImage: { type: String, default: "" },
    videoLengthTime: { type: String, default: "" },
    videoType: { type: String, enum: ["portrait", "landscape"] },
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
});

const Video = mongoose.model("Video", videoSchema);

module.exports = Video;