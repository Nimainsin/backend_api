const vacancy = require("./vacancy");
const query = require("./query");
const applyInVacancy = require("./applyInVacancy");
const user = require("./user");
const userRole = require("./userRole");
const rolePermission = require("./rolePermission");
const video = require("./video");
const questionCategory = require("./questionCategory");
const answer = require("./answer");
const village = require("./village");
const state = require("./state");
const energy = require("./energy");
const group = require("./group");
const district = require("./district");
const block = require("./block");
const gameContest = require("./gameContest");
const gameLevel = require("./gameLevel");
const obstacle = require("./obstacle");
const questionBank = require("./questionBank");
const memoryGame = require("./memoryGame");
const reward = require("./reward");
const role = require("./role");
const orgUser = require("./orgUser");
const orgUserRole = require("./orgUserRole");
const orgModule = require("./orgModule");
const otp = require("./otp");
const modulePriviledge = require("./modulePriviledge");
const login = require("./login");
const avatar = require("./avatar");
const winner = require("./winner");
const limit = require("./limit");
const questionBankHindi = require("./questionBankHindi");
const memoryGameHindi = require("./memoryGameHindi");
const rank = require("./rank");
const count = require("./count");
const chat = require("./chat");
const chatgrp = require("./chatgrp");
const orgModule2 = require("./orgModule2");

module.exports = {
  vacancy,
  query,
  applyInVacancy,
  user,
  userRole,
  rolePermission,
  video,
  questionCategory,
  answer,
  village,
  state,
  energy,
  group,
  district,
  block,
  gameContest,
  gameLevel,
  obstacle,
  questionBank,
  memoryGame,
  reward,
  role,
  orgUser,
  orgUserRole,
  orgModule,
  otp,
  modulePriviledge,
  login,
  avatar,
  winner,
  limit,
  questionBankHindi,
  memoryGameHindi,
  rank,
  count,
  chat,
  chatgrp,
  orgModule2
};