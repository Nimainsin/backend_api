// eslint-disable-next-line no-unused-vars
const MemoryGameHindi = require('./memoryGameHindiModel')

exports.insertMemoryGame = memory => MemoryGameHindi.create(memory)

exports.editMemoryGame = (memory, _id) =>
  MemoryGameHindi.findOneAndUpdate({ _id }, memory, { new: true })

exports.getAllMemoryGames = () =>
  MemoryGameHindi.find().populate({
    path: 'question',
    populate: {
      path: 'questionCategory',
    },
  })

exports.getMemoryGame = _id => MemoryGameHindi.findOne({ _id })

exports.deleteMemoryGame = _id => MemoryGameHindi.deleteOne({ _id })
