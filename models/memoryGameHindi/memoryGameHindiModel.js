const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const memoryGameHindiSchema = new mongoose.Schema(
    {
        question: [{ type: Schema.Types.ObjectId, ref: "QuestionBankHindi" }],
        name: { type: String, required: true },
    },
    {
        timestamps: true,
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                delete ret.__v;
                return ret;
            },
        },
    }
);

const MemoryGameHindi = mongoose.model("MemoryGameHindi", memoryGameHindiSchema);
MemoryGameHindi.createIndexes();
module.exports = MemoryGameHindi;
