// eslint-disable-next-line no-unused-vars
const MemoryGame = require("./memoryGameModel");

exports.insertMemoryGame = (memory) => MemoryGame.create(memory);

exports.editMemoryGame = (memory, _id) =>
  MemoryGame.findOneAndUpdate({ _id }, memory, { new: true });

exports.getAllMemoryGames = () =>
  MemoryGame.find().populate({
    path: "question",
    populate: {
      path: "questionCategory",
    },
  });

exports.getMemoryGame = (_id) => MemoryGame.findOne({ _id });

exports.deleteMemoryGame = (_id) => MemoryGame.deleteOne({ _id });
