const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const memoryGameSchema = new mongoose.Schema(
  {
    question: [{ type: Schema.Types.ObjectId, ref: "QuestionBank" }],
    name: { type: String, required: true },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const MemoryGame = mongoose.model("MemoryGame", memoryGameSchema);
MemoryGame.createIndexes();
module.exports = MemoryGame;
