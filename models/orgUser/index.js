// eslint-disable-next-line no-unused-vars
const OrgUser = require("./orgUserModel");

exports.insertOrgUser = (orgUser) => OrgUser.create(orgUser)

exports.getAllOrgUsers = () => OrgUser.find({ archive: false }).populate("role").populate("priviledge")

exports.getOrgUser = (_id) => OrgUser.findOne({ _id, archive: false }).populate("role").populate("priviledge")

exports.getOrgUserByEmail = (email) => OrgUser.findOne({ email })

exports.editOrgUser = (orgUser, _id) => OrgUser.findOneAndUpdate({ _id }, orgUser, { new: true })

exports.deleteOrgUser = (_id) => OrgUser.findOneAndUpdate({ _id }, { archive: true }, { new: true })


exports.undoOrgUser = (_id) => OrgUser.findOneAndUpdate({ _id }, { archive: false }, { new: true })