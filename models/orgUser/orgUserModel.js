const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const Schema = mongoose.Schema;
const orgUserSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: [{ type: Schema.Types.ObjectId, ref: "OrgUserRole" }],
    priviledge: { type: Schema.Types.ObjectId, ref: "ModulePriviledge" },
    archive: { type: Boolean, default: false },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const OrgUser = mongoose.model("OrgUser", orgUserSchema);
OrgUser.createIndexes();
module.exports = OrgUser;
