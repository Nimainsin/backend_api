const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const applySchema = new mongoose.Schema(
  {
    fullName: { type: String, required: true },
    email: { type: String, required: true },
    mobile: { type: Number, required: true },
    appliedPosition: { type: String, required: true },
    experience: { type: String, required: true },
    currentCompanyName: { type: String, required: true },
    currentSalary: { type: String, required: true },
    expectedSalary: { type: String, required: true },
    skills: { type: String, required: true },
    cvFile: { type: String, required: true },
  },
  { timestamps: true }
);

const Apply = mongoose.model("Apply", applySchema);

module.exports = Apply;
