const mongoose = require('kvell-db-plugin-mongoose').dbLib
const moduleSchema = new mongoose.Schema(
  {
    moduleName: { type: String, required: true },
    archive: { type: Boolean, default: false, required: true },
  },
  {
    timestamps: true,
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id
        delete ret._id
        delete ret.__v
        return ret
      },
    },
  }
)

const ModuleName = mongoose.model('ModuleName', moduleSchema)
ModuleName.createIndexes()
module.exports = ModuleName
