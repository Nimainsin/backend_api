// eslint-disable-next-line no-unused-vars
const OrgModule = require("./orgModuleModel");

exports.insertOrgModule = (orgModule) => OrgModule.create(orgModule)

exports.getAllOrgModules = () => OrgModule.find({ archive: false })

exports.getOrgModule = (_id) => OrgModule.findOne({ _id, archive: false })

exports.editOrgModule = (orgModule, _id) => OrgModule.findOneAndUpdate({ _id }, orgModule, { new: true })

exports.deleteOrgModule = (_id) => OrgModule.findOneAndUpdate({ _id }, { archive: true }, { new: true })