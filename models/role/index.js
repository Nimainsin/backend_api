// eslint-disable-next-line no-unused-vars
const Role = require("./roleModel");

exports.insertRole = (role) => Role.create(role)

exports.getRole = (name) => Role.findOne({ name })

exports.getAllRoles = () => Role.find()