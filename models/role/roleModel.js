const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const roleSchema = new mongoose.Schema(
    {
        name: { type: String, required: true, unique: true, enum: ["Admin", "Operator", "super Admin", "User", "Agent"] },
    },
    {
        timestamps: true,

    }
);

const Role = mongoose.model("Role", roleSchema);
Role.createIndexes()
module.exports = Role;