const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const energySchema = new mongoose.Schema(
  {
    // id: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    energyPoint: { type: Number, required: true },
  },
  {}
);

const Energy = mongoose.model("Energy", energySchema);
Energy.createIndexes();
module.exports = Energy;
