// eslint-disable-next-line no-unused-vars
const QuestionCategory = require("./questionCategoryModel");

exports.insertQuestionCategory = (questionCategory) => QuestionCategory.create(questionCategory)

exports.editQuestionCategory = (questionCategory, id) => QuestionCategory.findByIdAndUpdate({ _id: id }, questionCategory, { new: true })

exports.findQuestionCategory = (id) => QuestionCategory.findOne({ _id: id })

exports.findAllQuestionCategorys = () => QuestionCategory.find()

exports.removeQuestionCategory = (id) => QuestionCategory.remove({ _id: id })