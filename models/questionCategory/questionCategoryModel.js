
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const questionCategorySchema = new mongoose.Schema({
    name: { type: String, required: true, },
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
})

const QuestionCategory = mongoose.model("QuestionCategory", questionCategorySchema)

module.exports = QuestionCategory