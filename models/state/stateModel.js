
// Create your State model's schema here and export it.

const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const stateSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },

}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret,) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },
})

const State = mongoose.model("State", stateSchema)
State.createIndexes()
module.exports = State