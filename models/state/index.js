// eslint-disable-next-line no-unused-vars
const State = require("./stateModel");

exports.insertState = (state) => State.create(state)

exports.editState = (state, id) => State.findByIdAndUpdate({ _id: id }, state, { new: true })

exports.findState = (id) => State.findOne({ _id: id })

exports.findFilteredStates = (skip = 1, limit = 5) => State.find().skip(Number(skip)).limit(Number(limit))

exports.findAllStates = () => State.find()

exports.removeState = (id) => State.remove({ _id: id })

exports.countStatePages = () => State.count()