// eslint-disable-next-line no-unused-vars
const Obstacle = require("./obstacleModel");

exports.getAllObstacles = () => Obstacle.find()

exports.getObstacle = (_id) => Obstacle.findOne({ _id })

exports.createObstacle = (obstacle) => Obstacle.create(obstacle)

exports.editObstacle = (obstacle, _id) => Obstacle.findByIdAndUpdate({ _id }, obstacle, { new: true })

exports.deleteObstacle = (_id) => Obstacle.remove({ _id })