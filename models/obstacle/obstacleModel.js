
const mongoose = require("kvell-db-plugin-mongoose").dbLib;
const obstacleSchema = new mongoose.Schema({
    name: { type: String, required: true, },
    number: { type: Number, required: true },
    image: { type: String, default: "" },
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret,) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },

})

const Obstacle = mongoose.model("Obstacle", obstacleSchema)
Obstacle.createIndexes()
module.exports = Obstacle