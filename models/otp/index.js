// eslint-disable-next-line no-unused-vars
const Otp = require("./otpModel");

exports.createOtp = (otp) => Otp.create(otp)

exports.editOtp = (otp, id) => Otp.findByIdAndUpdate(id, otp, { new: true })

exports.getAllOtps = () => Otp.find()

exports.getOtp = (cond) => Otp.findOne(cond)

exports.deleteOtp = (_id) => Otp.deleteOne({ _id })