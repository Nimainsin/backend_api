
const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const otpSchema = new mongoose.Schema({
    mobile: { type: String, required: true },
    otp: { type: Number, required: true },
    sessionId: { type: String, required: true }
}, {
    timestamps: true,
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        },
    },

})

const Otp = mongoose.model("Otp", otpSchema)
Otp.createIndexes()
module.exports = Otp