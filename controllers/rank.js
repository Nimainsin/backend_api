

const rankController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const RankModelMethod = require("../models/rank");
const contestModelMethods = require("../models/gameContest");
const userModelMethods = require("../models/user");
const handlePagination = require("../helper/pagination");

rankController.insertRank = async (rank) => {
    try {
        let existedRank = await RankModelMethod.checkUniqueRank([rank.userId], [rank.contestId])
        if (existedRank) return { ...httpStatus.BAD_REQUEST, message: "Rank Already Exist!" }
        if (rank.userId) rank.user = [rank.userId]
        if (rank.contestId) rank.contest = [rank.contestId]

        let rankObj = await RankModelMethod.insertRank(rank);
        return { ...httpStatus.OK, rankObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};


rankController.editRank = async (rank, _id) => {
    try {
        console.log("inside rank");
        let existedRank = await RankModelMethod.findRank(_id)
        if (!existedRank) return { ...httpStatus.BAD_REQUEST, message: "Not Found !" }
        if (rank.userId) rank.user = [rank.userId]
        let rankObj = await RankModelMethod.editRank(rank, _id);
        return { ...httpStatus.OK, rankObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

rankController.getRank = async (_id) => {
    try {
        let rankObj = await RankModelMethod.findRank(_id);
        return { ...httpStatus.OK, rankObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};


rankController.getAllRanks = async ({ contestId, page, perPage }) => {
    try {
        let dataList = await RankModelMethod.findAllRanks(contestId)
        let finalData = []
        for (let data of dataList) {
            for (let con of data.contest) {
                for (let g of con.group) {
                    for (let u of g.user) {
                        if (String(u._id) === String(data.user[0]._id)) {
                            data._doc.userGroup = { ...g }
                            finalData.push(data)
                        }

                    }
                }

            }
        }


        if (page !== undefined && perPage !== undefined) {
            let totalPages = Math.ceil(finalData.length / perPage);
            let calculatedPage = (page - 1) * perPage;
            let calculatedPerPage = page * perPage;
            return {
                ...httpStatus.OK,
                data: dataList.slice(calculatedPage, calculatedPerPage),
                totalPages,
                allData: dataList,
                totalRecords: dataList.length,
            };
        } else {
            return {
                ...httpStatus.OK,
                data: dataList.slice(0, 4),
                allData: dataList,
                totalPages: Math.ceil(dataList.length / 4),
                totalRecords: dataList.length,
            };
        }
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error);
    }
};





rankController.getAllRank = async (id) => {
    try {
        let rankObj = await RankModelMethod.findAllRank(id);
        console.log(rankObj);
        let contestInfo = await contestModelMethods.findContest(id);
        let groupArr = contestInfo.group;
        let rankFreq = {};
        for (let rank of rankObj) {
            for (let group of groupArr) {
                for (let user of group.user) {
                    // console.log(user);
                    if (rank.userId === user.id) {
                        console.log(rank.userId === user.id, rank.userId);
                        if (rankFreq[group.name]) {
                            rankFreq[group.name] = rankFreq[group.name] + 1;
                        } else {
                            rankFreq[group.name] = 1;
                        }
                        break;
                    }
                }
            }
        }
        console.log(rankFreq);
        let rankGroupList = Object.keys(rankFreq).sort(function (a, b) {
            return rankFreq[b] - rankFreq[a];
        });
        console.log(rankGroupList);
        let rankGroupRank = [];
        for (let [i, grp] of rankGroupList.entries()) {
            rankGroupRank.push({ name: grp, position: i + 1 });
        }
        let playerRank = [];
        for (let [i, rank] of rankObj.entries()) {
            let user = await userModelMethods.findUser(rank.userId);

            let userObj = { position: i + 1, ...user._doc };
            playerRank.push(userObj);
        }
        return { ...httpStatus.OK, result: { rankGroupRank, playerRank } };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};



rankController.getAllPlayerRank = async ({ contestId, page, perPage }) => {
    try {
        let rankObj = await RankModelMethod.findAllRank(contestId);
        console.log(rankObj);
        let contestInfo = await contestModelMethods.findContest(contestId);
        let groupArr = contestInfo.group;
        let rankFreq = {};
        for (let rank of rankObj) {
            for (let group of groupArr) {
                for (let user of group.user) {
                    // console.log(user);
                    if (rank.userId === user.id) {
                        console.log(rank.userId === user.id, rank.userId);
                        if (rankFreq[group.name]) {
                            rankFreq[group.name] = rankFreq[group.name] + 1;
                        } else {
                            rankFreq[group.name] = 1;
                        }
                        break;
                    }
                }
            }
        }
        console.log(rankFreq);
        let rankGroupList = Object.keys(rankFreq).sort(function (a, b) {
            return rankFreq[b] - rankFreq[a];
        });
        console.log(rankGroupList);
        let rankGroupRank = [];
        for (let [i, grp] of rankGroupList.entries()) {
            rankGroupRank.push({ name: grp, position: i + 1 });
        }
        let playerRank = [];
        for (let [i, rank] of rankObj.entries()) {
            let user = await userModelMethods.findUser(rank.userId);
            //  let dataList = await RankModelMethod.findAllRank();
            let userObj = { position: i + 1, ...user._doc };
            playerRank.push(userObj);
        }
        // return { ...httpStatus.OK, result: { winnerGroupRank, playerRank } };
        let pagination = await handlePagination(playerRank, page, perPage);
        return { ...httpStatus.OK, ...pagination };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};


rankController.deleteRank = async (_id) => {
    try {
        let existedRank = await RankModelMethod.findRank(_id)
        if (!existedRank) return { ...httpStatus.BAD_REQUEST, message: "Not Found !" }
        let rankObj = await RankModelMethod.removeRank(_id);
        return { ...httpStatus.OK, rankObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

module.exports = rankController;