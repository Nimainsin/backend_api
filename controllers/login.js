/* eslint-disable no-unused-vars */
const loginController = {}
const httpStatus = require('../constants/httpStatus')
const userModelMethods = require('../models/user')
const orgUserModelMethods = require('../models/orgUser')
const hashingHandler = require('../utils/hashingHandler')
const jwtHandler = require('../utils/jwtHandler')
const errorHandler = require('../utils/errorHandler')
const randomize = require('randomatic')
const mailer = require('../constants/mailer')
const config = require('../config')
const otpModelMethod = require('../models/otp')
const { getSendOtp } = require('./sendOtp')

loginController.login = async ({ mobile, otp }) => {
  try {
    let userObj = await userModelMethods.findUserByMobile(mobile)
    if (!userObj)
      return {
        ...httpStatus.BAD_REQUEST,
        message: 'Please Sign Up before Login',
      }

    let otp = Math.round(Math.random() * 10000)
    // const sendOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/+91${mobile}/${otp}`;
    // let sentOtpResponse = await Axios.get(sendOtpApi);
    let sentOtpResponse = await getSendOtp(mobile, otp)
    if (sentOtpResponse.status === 200) {
      await otpModelMethod.createOtp({
        mobile,
        otp,
        sessionId: sentOtpResponse.data.Data[0].MessageId,
      })
      let res = await userModelMethods.editUser(
        {
          mobile,
          otpToken: otp,
        },
        userObj._id
      )
      return {
        ...httpStatus.OK,
        message: 'An OTP Sent Successfully On Your Registered Mobile!',
      }
    }

    //let otpObj = await otpModelMethod.createOtp()
    // if (!otpObj) return { ...httpStatus.BAD_REQUEST, message: "Incorrect Otp!" }
    // const verifyOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/VERIFY/${otpObj.sessionId}/${otp}`
    // let verifyOtpResponse = await Axios.get(verifyOtpApi)
    // if (verifyOtpResponse.data.Status === "Success") {

    //   let token = await jwtHandler.generateJWT(userObj, "24hr");
    //   return {
    //     ...httpStatus.OK,
    //     message: "Login Successfully!",
    //     token,
    //     userId: userObj.id,
    //   };
    // }
    // return { ...httpStatus.BAD_REQUEST, message: "Invalid OTP !" };
  } catch (error) {
    console.log(error)
    return httpStatus.INTERNAL_SERVER_ERROR
  }
}

loginController.orgUserLogin = async ({ email, password }) => {
  try {
    let userObj = await orgUserModelMethods.getOrgUserByEmail(email)
    if (!userObj)
      return {
        ...httpStatus.BAD_REQUEST,
        message: 'Invalid Email!',
      }

    let checkPass = await hashingHandler.comparingValue(password, userObj.password)
    if (!checkPass) return { ...httpStatus.NOT_FOUND, message: 'Incorrect Password!' }

    let token = await jwtHandler.generateJWT(userObj, '24hr')
    return {
      ...httpStatus.OK,
      message: 'Login Successfully!',
      token,
      userId: userObj.id,
    }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

loginController.orgUserForgetPass = async ({ email }) => {
  try {
    let userObj = await orgUserModelMethods.getOrgUserByEmail(email)
    if (!userObj)
      return {
        ...httpStatus.BAD_REQUEST,
        message: 'Your Email Is Not Registered!',
      }
    let password = randomize('*', 8)
    let hashedPassword = await hashingHandler.hashingValue(password)
    console.log(password, hashedPassword)
    let editedUserObj = await orgUserModelMethods.editOrgUser(
      { password: hashedPassword },
      userObj.id
    )
    console.log(editedUserObj)

    const { name } = userObj

    let message = `<div>
        <p> Dear <b>${name}</b>, </p>
        <p>Your Temporary Password Is <b>${password}</b></p><br>
        <p><i>This is a temporary password, reset after login</i></b></p>
        </div>`

    let mailOpts = {
      from: config.EMAIL_USER,
      to: email,
      subject: mailer.mailSubject.RESET_PASSWORD.subject,
      html: message,
    }

    mailer.smtpTrans.sendMail(mailOpts, (error, response) => {
      if (error) {
        console.log(error)
        return httpStatus.INTERNAL_SERVER_ERROR // Show a page indicating failure
      } else {
        console.log(response) // Show a page indicating success
        return { ...httpStatus.OK }
      }
    })
    return {
      ...httpStatus.OK,
      message:
        'A Temporary Password Has Sent To Your Registered Email, Just Login and Reset The Password !',
    }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = loginController
