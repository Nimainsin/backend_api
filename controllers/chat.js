
const chatController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const chatModelMethods = require("../models/chat");
const contestModelMethod = require("../models/gameContest")

chatController.insertChat = async (chat) => {
    try {
        let contestInfo = await contestModelMethod.findContest(chat.contestId)
        for (let g of contestInfo.group) {
            for (let u of g.user) {
                if (String(u._id) === String(chat.userId)) {
                    chat.contest = chat.contestId
                    chat.group = g.id
                    chat.user = chat.userId
                    let chatObj = await chatModelMethods.createChat(chat)
                    return { ...httpStatus.CREATED, chatObj }
                }

            }
        }

        return { ...httpStatus.NOT_FOUND, message: "User Not Present In the Contest" };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};



chatController.editChat = async (chat, id) => {
    try {
        let existedChat = await chatModelMethods.getChat(id)
        if (!existedChat) return httpStatus.NOT_FOUND
        let chatObj = await chatModelMethods.editChat(chat, id)
        return { ...httpStatus.UPDATED, chatObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

chatController.getChat = async (_id) => {
    try {
        let chatObj = await chatModelMethods.getChat(_id);
        if (!chatObj) return httpStatus.NOT_FOUND
        return { ...httpStatus.FETCHED, chatObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

chatController.getAllChats = async ({ contestId, groupId }) => {
    try {
        let cond = {}
        cond.contest = [contestId]
        cond.group = [groupId]
        let chatObj = await chatModelMethods.getAllChats(cond);
        return { ...httpStatus.FETCHED, chatObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

chatController.deleteChat = async (_id) => {
    try {
        let existedChat = await chatModelMethods.getChat(_id);
        if (!existedChat) return httpStatus.NOT_FOUND
        let chatObj = await chatModelMethods.deleteChat(_id)
        return { ...httpStatus.DELETED, chatObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};
module.exports = chatController;