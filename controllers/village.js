const villageController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const villageModelMethod = require("../models/village");
const handlePagination = require("../helper/pagination");

villageController.insertVillage = async (village) => {
  try {
    if (village.blockId) village.block = [village.blockId];
    let villageObj = await villageModelMethod.insertVillage(village);
    return { ...httpStatus.OK, villageObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

villageController.editVillage = async (Village, _id) => {
  try {
    if (Village.blockId) Village.block = [Village.blockId];
    let villageObj = await villageModelMethod.editVillage(Village, _id);
    return { ...httpStatus.OK, villageObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

villageController.getVillage = async (_id) => {
  try {
    let villageObj = await villageModelMethod.findVillage(_id);
    return { ...httpStatus.OK, villageObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

villageController.getAllVillage = async ({ page, perPage }) => {
  try {
    let villageObj = await villageModelMethod.findAllVillages();
    let pagination = await handlePagination(villageObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

villageController.getAllVillagesByBlock = async ({ page, perPage, blockId }) => {
  try {
    let villageObj = await villageModelMethod.findAllVillagesByBlock(blockId)
    let pagination = await handlePagination(villageObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

villageController.deleteVillage = async (_id) => {
  try {
    let villageObj = await villageModelMethod.removeVillage(_id);
    return { ...httpStatus.OK, villageObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = villageController;
