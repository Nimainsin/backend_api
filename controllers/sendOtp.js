const { default: axios } = require("axios");

const getSendOtp = async (mobile, otp) => {
  const sendOtpUrl = "http://101.53.153.17:6005//api/v2/SendSMS";
  const params = {
    SenderId: "OXFAMW",
    Is_Unicode: true,
    Is_Flash: false,
    // SchedTime: "string",
    // GroupId: "string",
    // Message: otp,
    Message: `Dear User, ${otp} is OTP for online game. OTPs are SECRET. DO NOT disclose it to anyone.Oxfam Team NEVER asks for OTP`,
    MobileNumbers: `91${mobile}`,
    // ServiceId: "string",
    // CoRelator: "string",
    // LinkId: "string",
    // PrincipleEntityId: "string",
    // TemplateId: "string",
    ApiKey: "rOgmKE2m5Jo3MWiAOhHqmcfxq8x5ayARttmnlLwI33k=",
    ClientId: "8f191fe2-8ec6-487a-81db-0fd06d10a77b",
  };
  let sentOtpResponse = await axios.get(sendOtpUrl, { params: params });
  return sentOtpResponse;
};

module.exports = {
  getSendOtp
}