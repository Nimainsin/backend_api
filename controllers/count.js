
const countController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const userModelMethod = require("../models/user")
const groupModelMethod = require("../models/group")
const contestModelMethod = require("../models/gameContest")


countController.getCount = async () => {
    try {
        let [userCount, groupCount, liveContestCount] = await Promise.all([
            userModelMethod.getUserCount(),
            groupModelMethod.getGroupCount(),
            contestModelMethod.getLiveContestCount()
        ])
        console.warn(userCount, groupCount, liveContestCount)
        return { ...httpStatus.FETCHED, result: { userCount, groupCount, liveContestCount } };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

module.exports = countController;