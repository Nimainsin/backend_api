const blockController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const blockModelMethod = require("../models/block");
const handlePagination = require("../helper/pagination");

blockController.insertBlock = async (block) => {
  try {
    block.district = [block.districtId];
    let blockObj = await blockModelMethod.createBlock(block);
    return { ...httpStatus.CREATED, blockObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

blockController.editBlock = async (block, _id) => {
  try {
    if (block.districtId) block.district = [block.districtId];

    console.log(block);
    let blockObj = await blockModelMethod.editBlock(block, _id);
    return { ...httpStatus.UPDATED, blockObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

blockController.getBlock = async (_id) => {
  try {
    let blockObj = await blockModelMethod.getBlock(_id);
    return { ...httpStatus.FETCHED, blockObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

blockController.getAllBlock = async ({ page, perPage }) => {
  try {
    let blockObj = await blockModelMethod.getAllBlocks();
    let pagination = await handlePagination(blockObj, page, perPage);
    return { ...httpStatus.FETCHED, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

blockController.getAllBlocksByDistrict = async ({ page, perPage, districtId }) => {
  try {
    let blockObj = await blockModelMethod.getAllBlocksByDistrict(districtId);
    let pagination = await handlePagination(blockObj, page, perPage);
    return { ...httpStatus.FETCHED, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

blockController.deleteBlock = async (_id) => {
  try {
    let blockObj = await blockModelMethod.deleteBlock(_id);
    return { ...httpStatus.DELETED, blockObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = blockController;
