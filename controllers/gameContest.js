const gameContestController = {}
const errorHandler = require('../utils/errorHandler')
const httpStatus = require('../constants/httpStatus')
const gameContestModelMethod = require('../models/gameContest')
const handlePagination = require('../helper/pagination')
const translate = require('@vitalets/google-translate-api')

gameContestController.insertContest = async contest => {
  try {
    contest.group = contest.groupIds
    let contestCode = Math.floor(100000 + Math.random() * 900000)
    contestCode = contestCode.toString().substring(0, 6)
    contestCode = parseInt(contestCode)
    contest.contestCode = contestCode
    let contestObj = await gameContestModelMethod.insertContest(contest)
    return {
      ...httpStatus.OK,
      message: 'Content Created Successfully!',
      contestObj,
    }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.editContest = async (contest, id) => {
  try {
    let existedContest = await gameContestModelMethod.findContest(id)
    if (!existedContest)
      return {
        ...httpStatus.BAD_REQUEST,
        message: "The Record You Want To Delete, Doesn't Exist",
      }
    if (contest.groupIds) contest.group = contest.groupIds
    let contestObj = await gameContestModelMethod.editContest(contest, id)
    return {
      ...httpStatus.OK,
      message: 'Content Updated Successfully!',
      contestObj,
    }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.getContest = async _id => {
  try {
    let ContestObj = await gameContestModelMethod.findContest(_id)
    return { ...httpStatus.OK, ContestObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.getAllContest = async ({ page = 1, perPage = 4 }) => {
  try {
    let ContestObj = await gameContestModelMethod.findAllContests()
    let pagination = await handlePagination(ContestObj, page, perPage)
    return { ...httpStatus.OK, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.getAllContestInHindi = async ({ page = 1, perPage = 4 }) => {
  try {
    let ContestObj = await gameContestModelMethod.findAllContests()
    const dupContestObj = ContestObj.slice()
    let hindiObj = []
    for (let con of dupContestObj) {
      const description = await translate(con.description, { from: 'en', to: 'hi' })
      console.log(description.text)
      con.description = description.text

      const name = await translate(con.name, { from: 'en', to: 'hi' })
      console.log(name.text)
      con.name = name.text

      hindiObj.push(con)
    }
    // console.log(hindiObj)git

    let pagination = await handlePagination(hindiObj, page, perPage)
    return { ...httpStatus.OK, ...pagination }
  } catch (error) {
    console.log(error)
    return httpStatus.INTERNAL_SERVER_ERROR
  }
}
gameContestController.deleteContest = async _id => {
  try {
    let ContestObj = await gameContestModelMethod.removeContest(_id)
    return { ...httpStatus.OK, ContestObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.getContestsByUserId = async id => {
  try {
    let contestObj = await gameContestModelMethod.findAllContests()
    let filteredContests = []
    contestObj.forEach(contest => {
      contest.group.forEach(grp => {
        grp.user.forEach(usr => {
          if (usr.id === id) {
            filteredContests.push(contest)
            return
          }
        })
      })
    })

    filteredContests = [...new Set(filteredContests)]
    return { ...httpStatus.OK, contestObj: filteredContests }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

gameContestController.translateText = async ({ text, from = 'en', to = 'hi' }) => {
  try {
    const data = await translate(text, { from, to })
    return { ...httpStatus.OK, data: data, text: text, from, to }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = gameContestController
