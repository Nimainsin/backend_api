const orgUserController = {};
const mongoose = require("mongoose");
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const OrgUserModelMethod = require("../models/orgUser");
const modulePriviledgeMethod = require("../models/modulePriviledge");
const hashingHandler = require("../utils/hashingHandler");
const jwtHandler = require("../utils/jwtHandler");
const Promise = require("bluebird");

orgUserController.insertOrgUser = async (OrgUser) => {
  try {
    let existedOrgUser = await OrgUserModelMethod.getOrgUserByEmail(
      OrgUser.email
    );
    if (existedOrgUser)
      return {
        ...httpStatus.BAD_REQUEST,
        message: "Email Already Registered!",
      };
    OrgUser.role = [OrgUser.roleId];
    OrgUser.password = await hashingHandler.hashingValue(OrgUser.password);
    let priviledgeArrObj = OrgUser.priviledge;
    delete OrgUser.priviledge;
    let orgUserObj = await OrgUserModelMethod.insertOrgUser(OrgUser);
    priviledgeArrObj = priviledgeArrObj.map((pri) => {
      pri.permissionAssignTo = orgUserObj._id;
      // element.permissionAssignedBy=user._id
      pri.permissionAssignedBy = "";
      return pri;
    });
    let priviledge = await modulePriviledgeMethod.createModulePriviledgeInBulk(
      priviledgeArrObj
    );
    orgUserObj = { ...orgUserObj._doc, priviledge };
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.editOrgUser = async (OrgUser, _id) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(_id))
      return { ...httpStatus.BAD_REQUEST, message: "UserId Not Exist!" };
    let existedUser = await OrgUserModelMethod.getOrgUser(_id);
    if (!existedUser)
      return { ...httpStatus.BAD_REQUEST, message: "User Not Found!" };
    if (OrgUser.roleId) OrgUser.role = [OrgUser.roleId];

    if (OrgUser.password)
      OrgUser.password = await hashingHandler.hashingValue(OrgUser.password);
    let priviledgeArrObj = OrgUser.priviledge;
    delete OrgUser.priviledge;
    let orgUserObj = await OrgUserModelMethod.editOrgUser(OrgUser, _id);
    await modulePriviledgeMethod.deleteModulePriviledgeInBulk(_id);

    priviledgeArrObj = priviledgeArrObj.map((pri) => {
      pri.permissionAssignTo = orgUserObj._id;
      // element.permissionAssignedBy=user._id
      pri.permissionAssignedBy = "";
      return pri;
    });
    let priviledge = await modulePriviledgeMethod.createModulePriviledgeInBulk(
      priviledgeArrObj
    );
    orgUserObj = { ...orgUserObj._doc, priviledge };
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.getOrgUser = async (_id) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(_id))
      return { ...httpStatus.BAD_REQUEST, message: "UserId Not Exist!" };
    let existedUser = await OrgUserModelMethod.getOrgUser(_id);
    if (!existedUser)
      return { ...httpStatus.BAD_REQUEST, message: "User Not Found!" };

    let orgUserObj = await OrgUserModelMethod.getOrgUser(_id);
    if (!orgUserObj)
      return { ...httpStatus.BAD_REQUEST, message: "UserId Not Exist!" };

    let priviledge = await modulePriviledgeMethod.getModulePriviledge(_id);
    orgUserObj = { ...orgUserObj._doc, priviledge };
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.getOrgUserByToken = async ({ token }) => {
  try {
    let orgUserObj = jwtHandler.validateJWT(token);
    console.log('Texttttttt',orgUserObj);
    
    orgUserObj = orgUserObj._doc;
    console.log('Textttttttoneeee',orgUserObj);
    let priviledge = await modulePriviledgeMethod.getModulePriviledge(
      orgUserObj._id
    );

    console.log("id",orgUserObj._id)
    delete orgUserObj.password;
    orgUserObj = { ...orgUserObj, priviledge };
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.getAllOrgUser = async () => {
  try {
    let orgUserObj = await OrgUserModelMethod.getAllOrgUsers();
    let updatedOrgUser = [];
    return await Promise.each(orgUserObj, async (user) => {
      let modelPriviledgeData = await modulePriviledgeMethod.getModulePriviledge(
        user._id
      );
      user = await { ...user._doc, priviledge: modelPriviledgeData };
      await updatedOrgUser.push(user);
    }).then(async () => {
      return await { ...httpStatus.OK, orgUserObj: updatedOrgUser };
    });
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.deleteOrgUser = async (_id) => {
  try {
    let orgUserObj = await OrgUserModelMethod.deleteOrgUser(_id);
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgUserController.undoOrgUser = async (_id) => {
  try {
    let orgUserObj = await OrgUserModelMethod.undoOrgUser(_id);
    return { ...httpStatus.OK, orgUserObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = orgUserController;
