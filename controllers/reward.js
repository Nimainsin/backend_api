const rewardController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const fileHandler = require("../utils/fileHandler");
const rewardModelMethod = require("../models/reward");
const path = require("path");
const fs = require("fs");
const handlePagination = require("../helper/pagination");

rewardController.insertReward = async ({ fields, files }) => {
  try {
    let Reward = fields;
    let file = files.image;

    // create directories if doesn't exist

    let dir = path.join(__dirname, "..", "public");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    dir = path.join(__dirname, "..", "public", "RewardImages");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    if (file) {
      let fileName = path.join(
        "RewardImages",
        `RewardImage_${Date.now()}${path.extname(file.name)}`
      );
      let newPath = path.join(__dirname, "..", "public", fileName);
      let oldPath = file.path;

      let fileInfo = await fileHandler.filePathHandler(
        newPath,
        fileName,
        oldPath
      );
      Reward.image = fileInfo.fileName;
    }

    let rewardObj = await rewardModelMethod.insertReward(Reward);
    return { ...httpStatus.OK, rewardObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

rewardController.editReward = async ({ fields, files, _id }) => {
  try {
    let Reward = fields;
    let file = files.image;

    // create directories if doesn't exist
    let newFile = false;
    let dir = path.join(__dirname, "..", "public");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    dir = path.join(__dirname, "..", "public", "RewardImages");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    if (file) {
      let fileName = path.join(
        "RewardImages",
        `RewardImage_${Date.now()}${path.extname(file.name)}`
      );
      let newPath = path.join(__dirname, "..", "public", fileName);
      let oldPath = file.path;

      let fileInfo = await fileHandler.filePathHandler(
        newPath,
        fileName,
        oldPath
      );
      Reward.image = fileInfo.fileName;
      newFile = true;
    }
    let existedReward = await rewardModelMethod.getReward(_id);

    if (existedReward && existedReward.image && newFile) {
      let filePathToDelete = path.join(
        __dirname,
        "..",
        "public",
        existedReward.image
      );

      if (fs.existsSync(filePathToDelete))
        await fileHandler.unlinkFileHandler(filePathToDelete);
    }

    let rewardObj = await rewardModelMethod.editReward(Reward, _id);
    return { ...httpStatus.OK, rewardObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

rewardController.getReward = async (_id) => {
  try {
    let rewardObj = await rewardModelMethod.getReward(_id);
    return { ...httpStatus.OK, rewardObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

rewardController.getAllReward = async ({ page, perPage }) => {
  try {
    let rewardObj = await rewardModelMethod.getAllRewards();
    let pagination = await handlePagination(rewardObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

rewardController.deleteReward = async (id) => {
  try {
    let rewardObj = await rewardModelMethod.deleteReward(id);
    return { ...httpStatus.OK, rewardObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};
module.exports = rewardController;
