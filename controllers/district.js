const districtController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const districtModelMethod = require("../models/district");
const handlePagination = require("../helper/pagination");

districtController.insertDistrict = async (district) => {
  try {
    district.state = [district.stateId];
    let districtObj = await districtModelMethod.createDistrict(district);
    return { ...httpStatus.CREATED, districtObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

districtController.editDistrict = async (district, _id) => {
  try {
    if (district.state) district.state = [district.stateId];
    let districtObj = await districtModelMethod.editDistrict(district, _id);
    return { ...httpStatus.UPDATED, districtObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

districtController.getDistrict = async (_id) => {
  try {
    let districtObj = await districtModelMethod.getDistrict(_id);
    return { ...httpStatus.FETCHED, districtObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

districtController.getAllDistrict = async ({ page, perPage }) => {
  try {
    let districtObj = await districtModelMethod.getAllDistricts();
    let pagination = await handlePagination(districtObj, page, perPage);
    return { ...httpStatus.FETCHED, ...pagination };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

districtController.deleteDistrict = async (_id) => {
  try {
    let districtObj = await districtModelMethod.deleteDistrict(_id);
    return { ...httpStatus.DELETED, districtObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

districtController.getAllDistrictsByState = async ({ page, perPage, stateId }) => {
  try {
    let districtObj = await districtModelMethod.getDistrictByStateId(stateId);
    let pagination = await handlePagination(districtObj, page, perPage);
    return { ...httpStatus.FETCHED, ...pagination };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = districtController;
