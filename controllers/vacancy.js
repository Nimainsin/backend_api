
const vacancyController = {};
const errorHandler = require("../utils/errorHandler")
const httpStatus = require("../constants/httpStatus")
const vacancyModelMethods = require("../models/vacancy")
const applyModelMethod = require("../models/applyInVacancy")
const fileHandler = require("../utils/fileHandler")
const path = require("path")
const fs = require("fs")

vacancyController.insertVacancy = async (vacancy) => {
    try {
        let vacancyObj = await vacancyModelMethods.insertVacancy(vacancy)
        return { ...httpStatus.OK, vacancyObj }

    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

vacancyController.editVacancy = async (vacancy, _id) => {
    try {
        let vacancyObj = await vacancyModelMethods.editVacancy(vacancy, _id)
        return { ...httpStatus.OK, vacancyObj }

    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

vacancyController.getVacancy = async (_id) => {
    try {
        let vacancyObj = await vacancyModelMethods.getVacancy(_id)
        return { ...httpStatus.OK, vacancyObj }

    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

vacancyController.getAllVacancy = async () => {
    try {
        let vacancyObj = await vacancyModelMethods.getAllVacancy()
        return { ...httpStatus.OK, vacancyObj }

    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}


vacancyController.deleteVacancy = async (_id) => {
    try {
        let vacancyObj = await vacancyModelMethods.deleteVacancy(_id)
        return { ...httpStatus.OK, vacancyObj }

    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

vacancyController.applyForVacancy = async ({ fields, files }) => {
    try {
        let apply = fields
        let file = files.cvFile
        // create directories if doesn't exist
        if (file) {
            let dir = path.join(__dirname, "..", "public");
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            dir = path.join(__dirname, "..", "public", "cv");
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            let fileName = path.join("cv", `cv_${apply.email}_${Date.now()}${path.extname(file.name)}`);
            let newPath = path.join(__dirname, "..", "public", fileName);
            let oldPath = file.path;

            let fileInfo = await fileHandler.filePathHandler(newPath, fileName, oldPath);
            apply.cvFile = fileInfo.fileName;
        }
        let applyObj = await applyModelMethod.insertApply(apply)

        return { ...httpStatus.OK, message: "Information Inserted Successfully", applyObj }
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error)
    }
}

module.exports = vacancyController;