const gameLevelController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const gameLevelModelMethod = require("../models/gameLevel");

gameLevelController.insertGameLevel = async (gameLevel) => {
  try {
    gameLevel.obstacle = gameLevel.obstacleIds;
    gameLevel.reward = gameLevel.rewardIds;
    gameLevel.memoryGame = gameLevel.memoryGameIds;
    gameLevel.contest = gameLevel.contestIds;
    let gameLevelObj = await gameLevelModelMethod.insertGameLevel(gameLevel);
    return { ...httpStatus.OK, gameLevelObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

gameLevelController.editGameLevel = async (gameLevel, _id) => {
  try {
    if (gameLevel.rewardIds) gameLevel.reward = gameLevel.rewardIds;
    if (gameLevel.memoryGameIds) gameLevel.memoryGame = gameLevel.memoryGameIds;
    if (gameLevel.contestIds) gameLevel.contest = gameLevel.contestIds;
    if (gameLevel.obstacleIds) gameLevel.obstacle = gameLevel.obstacleIds;

    let gameLevelObj = await gameLevelModelMethod.editGameLevel(gameLevel, _id);
    return { ...httpStatus.OK, gameLevelObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

gameLevelController.getGameLevel = async (_id) => {
  try {
    let gameLevelObj = await gameLevelModelMethod.findGameLevel(_id);
    return { ...httpStatus.OK, gameLevelObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

gameLevelController.getAllGameLevel = async () => {
  try {
    let gameLevelObj = await gameLevelModelMethod.findAllGameLevels();
    return { ...httpStatus.OK, gameLevelObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

gameLevelController.deleteGameLevel = async (_id) => {
  try {
    let gameLevelObj = await gameLevelModelMethod.removeGameLevel(_id);
    return { ...httpStatus.OK, gameLevelObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = gameLevelController;
