
const otpController = {};
const otpModelMethod = require("../models/otp")
const httpStatus = require("../constants/httpStatus")
const errorHandler = require("../utils/errorHandler");
const customHttpStatus = require("../constants/customHttpStatus");
const config = require("../config")
const Axios = require("axios")


otpController.createOtp = async ({ mobile }) => {
    try {
        let otp = Math.round(Math.random() * 10000)
        const sendOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/+91${mobile}/${otp}`
        let sentOtpResponse = await Axios.get(sendOtpApi)
        await otpModelMethod.createOtp({ mobile, otp, sessionId: sentOtpResponse.data.Details })
        return { ...httpStatus.OK, message: "An OTP Sent Successfully On Your Registered Mobile!" }
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

otpController.verifyOtp = async ({ mobile, otp }) => {
    try {
        let otpObj = await otpModelMethod.getOtp({ mobile, otp })
        if (!otpObj) return { ...httpStatus.BAD_REQUEST, message: "Incorrect Otp!" }
        const verifyOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/VERIFY/${otpObj.sessionId}/${otp}`
        let verifyOtpResponse = await Axios.get(verifyOtpApi)
        return { ...httpStatus.OK, message: verifyOtpResponse.data.Details }
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

otpController.editOtp = async (otp, id) => {
    try {
        await otpModelMethod.editOtp(otp, id)
        return customHttpStatus.UPDATED
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

otpController.getAllOtps = async () => {
    try {
        let otpObj = await otpModelMethod.getAllOtps()
        return { ...customHttpStatus.FETCHED, otpObj }
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

otpController.getOtp = async (id) => {
    try {
        let otpObj = await otpModelMethod.getOtp(id)
        return { ...customHttpStatus.FETCHED, otpObj }
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}

otpController.deleteOtp = async (id) => {
    try {
        let otpObj = await otpModelMethod.deleteOtp(id)
        if (otpObj.n > 0) return customHttpStatus.DELETED
        return customHttpStatus.INTERNAL_SERVER_ERROR
    } catch (error) {
        console.log(error)
        return errorHandler.errorHandlerMain(error)
    }
}
module.exports = otpController;