const answerController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const answerModelMethod = require("../models/answer");

answerController.insertAnswer = async (answer) => {
  try {
    let answerObj = await answerModelMethod.insertAnswer(answer);
    return { ...httpStatus.OK, answerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

answerController.editAnswer = async (answer, _id) => {
  try {
    let answerObj = await answerModelMethod.editAnswer(answer, _id);
    return { ...httpStatus.OK, answerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

answerController.getAnswer = async (_id) => {
  try {
    let answerObj = await answerModelMethod.findAnswer(_id);
    return { ...httpStatus.OK, answerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

answerController.getAllAnswer = async () => {
  try {
    let answerObj = await answerModelMethod.findAllAnswers();
    return { ...httpStatus.OK, answerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

answerController.deleteAnswer = async (_id) => {
  try {
    let answerObj = await answerModelMethod.removeAnswer(_id);
    return { ...httpStatus.OK, answerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = answerController;
