const stateController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const stateModelMethod = require("../models/state");
const handlePagination = require("../helper/pagination");

stateController.insertState = async (state) => {
  try {
    let stateObj = await stateModelMethod.insertState(state);
    return { ...httpStatus.OK, stateObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

stateController.editstate = async (state, _id) => {
  try {
    let stateObj = await stateModelMethod.editState(state, _id);
    return { ...httpStatus.OK, stateObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

stateController.getstate = async (_id) => {
  try {
    let stateObj = await stateModelMethod.findState(_id);
    return { ...httpStatus.OK, stateObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

stateController.getAllstate = async ({ page, perPage }) => {
  try {
    let stateObj = await stateModelMethod.findAllStates();
    let pagination = await handlePagination(stateObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }

  // try {
  //     let data = await stateModelMethod.findFilteredStates(skip, limit)
  //     let allData = await stateModelMethod.findAllStates()
  //     let pages = await stateModelMethod.countStatePages()
  //     if (!limit) limit = 5
  //     if (!skip) skip = 1
  //     let totalPages = Math.ceil(pages / limit)
  //     return { ...httpStatus.OK, allData, data, totalPages, totalRecords: pages, limit, skip }

  // } catch (error) {
  //     console.log(error)
  //     return errorHandler.errorHandlerMain(error)
  // }
};

stateController.deletestate = async (_id) => {
  try {
    let stateObj = await stateModelMethod.removeState(_id);
    return { ...httpStatus.OK, stateObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = stateController;
