const orgModule2Controller = {}
const errorHandler = require('../utils/errorHandler')
const httpStatus = require('../constants/httpStatus')
const OrgModuleModelMethod = require('../models/orgModule2')
const handlePagination = require('../helper/pagination')

orgModule2Controller.insertOrgModule = async OrgModule => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.insertOrgModule(OrgModule)
    return { ...httpStatus.OK, orgModuleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgModule2Controller.editOrgModule = async (OrgModule, _id) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.editOrgModule(OrgModule, _id)
    return { ...httpStatus.OK, orgModuleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgModule2Controller.getOrgModule = async _id => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.getOrgModule(_id)
    return { ...httpStatus.OK, orgModuleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgModule2Controller.getAllOrgModule = async ({ page, perPage }) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.getAllOrgModules()
    let pagination = await handlePagination(orgModuleObj, page, perPage)
    return { ...httpStatus.OK, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgModule2Controller.deleteOrgModule = async _id => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.deleteOrgModule(_id)
    return { ...httpStatus.OK, orgModuleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = orgModule2Controller
