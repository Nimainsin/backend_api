const questionCategoryController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const questionCategoryModelMethods = require("../models/questionCategory");
const handlePagination = require("../helper/pagination");

questionCategoryController.insertQuestionCategory = async (
  questionCategory
) => {
  try {
    let questionCatObj = await questionCategoryModelMethods.insertQuestionCategory(
      questionCategory
    );
    return { ...httpStatus.OK, questionCatObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

questionCategoryController.editQuestionCategory = async (user, _id) => {
  try {
    let userObj = await questionCategoryModelMethods.editQuestionCategory(
      user,
      _id
    );
    return { ...httpStatus.OK, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

questionCategoryController.getQuestionCategory = async (_id) => {
  try {
    let userObj = await questionCategoryModelMethods.findQuestionCategory(_id);
    return { ...httpStatus.OK, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

questionCategoryController.getAllQuestionCategory = async ({
  page,
  perPage,
}) => {
  try {
    let questionCategoryObj = await questionCategoryModelMethods.findAllQuestionCategorys();
    let pagination = await handlePagination(questionCategoryObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

questionCategoryController.deleteQuestionCategory = async (_id) => {
  try {
    let userObj = await questionCategoryModelMethods.removeQuestionCategory(
      _id
    );
    return { ...httpStatus.OK, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = questionCategoryController;
