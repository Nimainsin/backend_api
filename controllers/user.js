/* eslint-disable no-unused-vars */
const userController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const userModelMethods = require("../models/user");
const avatarModelMethod = require("../models/avatar");


userController.insertUser = async (user) => {
  try {
    let userObj = await userModelMethods.insertUser(user);
    console.log(userObj);
    return { ...httpStatus.CREATED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.editUser = async (user, _id) => {
  try {
    // if (user.role) {
    //     let userRoleObj = await userRoleModalMethods.getUserRole(user.role)
    //     console.log(userRoleObj)
    //     user.role = [userRoleObj.id]
    // }
    if (user.villageId) user.village = user.villageId;
    if (user.energy) {
      user.energy = [user.energy];
    }
    let userObj = await userModelMethods.editUser(user, _id);

    return { ...httpStatus.UPDATED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.deactivateUser = async (_id) => {
  try {
    let userObj = await userModelMethods.editUser({ status: "inactive" }, _id);
    return { ...httpStatus.UPDATED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.setAvatar = async ({ userId, avatarId }) => {
  try {
    let avatarObj = await avatarModelMethod.getAvatar(avatarId);
    await userModelMethods.editUser({ imageUrl: avatarObj.name }, userId);
    return { ...httpStatus.UPDATED, message: "Avatar Set Successfully!" };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.getUser = async (_id) => {
  try {
    let userObj = await userModelMethods.findUser(_id);
    return { ...httpStatus.FETCHED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.getUserByVillage = async (_id) => {
  try {
    let userObj = await userModelMethods.findUserByVillage(_id);
    return { ...httpStatus.FETCHED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.getAllUser = async () => {
  try {
    let userObj = await userModelMethods.findAllUsers();
    return { ...httpStatus.FETCHED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

userController.deleteUser = async (_id) => {
  try {
    let userObj = await userModelMethods.removeUser(_id);
    return { ...httpStatus.DELETED, userObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = userController;
