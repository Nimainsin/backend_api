const modulePriviledgeController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const modulePriviledgeModelMethod = require("../models/modulePriviledge");
const orgUserModel = require("../models/orgUser");
const handlePagination = require("../helper/pagination");

modulePriviledgeController.insertModulePriviledge = async (priviledge) => {
  try {
    priviledge.module = priviledge.moduleId
    let modulePriviledgeObj = await modulePriviledgeModelMethod.createModulePriviledge(
      priviledge
    );
    console.log(modulePriviledgeObj);
    let orgUser = await orgUserModel.editOrgUser(
      { priviledge: [modulePriviledgeObj.id] },
      priviledge.permissionAssignTo
    );
    console.log(orgUser);
    return { ...httpStatus.OK, modulePriviledgeObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

modulePriviledgeController.editModulePriviledge = async (priviledge, _id) => {
  try {
    let modulePriviledgeObj = await modulePriviledgeModelMethod.editModulePriviledge(
      priviledge,
      _id
    );
    return { ...httpStatus.OK, modulePriviledgeObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

modulePriviledgeController.getModulePriviledge = async (_id) => {
  try {
    let modulePriviledgeObj = await modulePriviledgeModelMethod.getModulePriviledge(
      _id
    );
    return { ...httpStatus.OK, modulePriviledgeObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

modulePriviledgeController.getAllModulePriviledge = async ({
  page,
  perPage,
}) => {
  try {
    let modulePriviledgeObj = await modulePriviledgeModelMethod.getAllModulePriviledges();
    let pagination = await handlePagination(modulePriviledgeObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

modulePriviledgeController.deleteModulePriviledge = async (_id) => {
  try {
    let modulePriviledgeObj = await modulePriviledgeModelMethod.deleteModulePriviledge(
      _id
    );
    return { ...httpStatus.OK, modulePriviledgeObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = modulePriviledgeController;
