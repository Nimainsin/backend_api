/* eslint-disable no-unused-vars */
const signUpController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const userModelMethods = require("../models/user");
const otmControllermethod = require("../controllers/otp");
const config = require("../config");
const otpModelMethod = require("../models/otp");
const { default: axios } = require("axios");
const { debug } = require("request");
const { getSendOtp } = require("./sendOtp");

signUpController.signUp = async ({ mobile }) => {
  try {
    const existedUser = await userModelMethods.findUserByMobile(mobile);
    if (existedUser) {
      return { ...httpStatus.BAD_REQUEST, message: "User Already Exist!" };
    }
    let otp = Math.round(Math.random() * 10000);
    //const sendOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/+91${mobile}/${otp}`;
    //let sentOtpResponse = await Axios.get(sendOtpApi);
    // if (sentOtpResponse.data.Status === "Success") {
    //   await otpModelMethod.createOtp({
    //     mobile,
    //     otp,
    //     sessionId: sentOtpResponse.data.Details,
    //   });
    //   let userObj = await userModelMethods.insertUser({
    //     mobile,
    //     otpToken: otp,
    //   });
    //   console.log(userObj);
    //   return {
    //     ...httpStatus.OK,
    //     message: "An OTP Sent Successfully On Your Registered Mobile!",
    //   };
    // }

    let sentOtpResponse = await getSendOtp(mobile, otp);
    if (sentOtpResponse.status === 200) {
      await otpModelMethod.createOtp({
        mobile,
        otp,
        sessionId: sentOtpResponse.data.Data[0].MessageId,
      });
      let userObj = await userModelMethods.insertUser({
        mobile,
        otpToken: otp,
      });
      console.log(userObj);
      return {
        ...httpStatus.OK,
        message: "An OTP Sent Successfully On Your Registered Mobile!",
      };
    }
    return await {
      ...httpStatus.BAD_REQUEST,
      message: "Incorrect Otp!",
    };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

signUpController.verify = async ({ mobile, otpToken }) => {
  try {
    let userObj = await userModelMethods.findUserByFilter({ mobile, otpToken });
    if (!userObj)
      return {
        ...httpStatus.BAD_REQUEST,
        message: "Mobile Number or OTP is Incorrect!",
      };
    let otpObj = await otpModelMethod.getOtp({ mobile, otp: otpToken });
    if (!otpObj)
      return { ...httpStatus.BAD_REQUEST, message: "Incorrect Otp!" };
    // const verifyOtpApi = `https://2factor.in/API/V1/${config.OTP_API_KEY}/SMS/VERIFY/${otpObj.sessionId}/${otpToken}`;
    // let verifyOtpResponse = await axios.get(verifyOtpApi);
    // console.log(verifyOtpResponse.data);
    if (userObj && otpObj) {
      let user = {};
      user.isMobileVerified = true;
      //user.status = "active";
      let editedUser = await userModelMethods.editUser(user, userObj.id);
      return { ...httpStatus.OK, userObj: editedUser };
    }
    return { ...httpStatus.BAD_REQUEST, message: "Otp Not Verified!" };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};
module.exports = signUpController;
