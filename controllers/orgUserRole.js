const orgUserRoleController = {}
const orgModule2ModalMethods = require('../models/orgModule2')
const errorHandler = require('../utils/errorHandler')
const httpStatus = require('../constants/httpStatus')
const OrgUserRoleModelMethod = require('../models/orgUserRole')
const handlePagination = require('../helper/pagination')

orgUserRoleController.insertOrgUserRole = async OrgUserRole => {
  try {
    let orgUserRoleObj = await OrgUserRoleModelMethod.insertRole(OrgUserRole)
    return { ...httpStatus.OK, orgUserRoleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgUserRoleController.editOrgUserRole = async (OrgUserRole, _id) => {
  try {
    let orgModuleRoleData = OrgUserRole.orgModuleRoleData
    delete OrgUserRole.orgModuleRoleData
    let orgUserRoleObj = await OrgUserRoleModelMethod.editRole(OrgUserRole, _id)
    console.log('orgModuleRoleData', orgModuleRoleData)
    let promiseArray = orgModuleRoleData.map(m => {
      // {"id": "60c8dba5a48fe40022494865", "roles" : ["5fed3cce77aff72b59b1e72c","5fed3f4609037c2f7efbea40"]}
      const { id, roles } = m
      return orgModule2ModalMethods.editOrgModule({ roles }, id)
    })
    console.log('promiseArray', promiseArray)
    Promise.all(promiseArray).then(res => console.log(res))
    return { ...httpStatus.OK, orgUserRoleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgUserRoleController.getOrgUserRole = async _id => {
  try {
    let orgUserRoleObj = await OrgUserRoleModelMethod.getRole(_id)
    return { ...httpStatus.OK, orgUserRoleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgUserRoleController.getAllOrgUserRole = async ({ page, perPage }) => {
  try {
    let orgUserRoleObj = await OrgUserRoleModelMethod.getAllRoles()
    //return { ...httpStatus.OK, orgUserRoleObj };
    let pagination = await handlePagination(orgUserRoleObj, page, perPage)
    return { ...httpStatus.OK, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

orgUserRoleController.deleteOrgUserRole = async _id => {
  try {
    let orgUserRoleObj = await OrgUserRoleModelMethod.deleteRole(_id)
    return { ...httpStatus.OK, orgUserRoleObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = orgUserRoleController
