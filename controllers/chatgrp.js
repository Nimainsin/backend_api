
const chatgrpController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/customHttpStatus");
const chatgrpModelMethods = require("../models/chatgrp");
const contestModelMethod = require("../models/gameContest")

chatgrpController.insertgrpChat = async (chat) => {
    try {
        let contestInfo = await contestModelMethod.findContest(chat.contestId)
        for (let g of contestInfo.group) {
            for (let u of g.user) {
                if (String(u._id) === String(chat.userId)) {
                    chat.contest = chat.contestId
                    chat.group = g.id
                    chat.user = chat.userId
                    let chatObj = await chatgrpModelMethods.creategrpChat(chat)
                    return { ...httpStatus.CREATED, chatObj }
                }

            }
        }

        return { ...httpStatus.NOT_FOUND, message: "User Not Present In the Contest" };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};





chatgrpController.getAllgrpChats = async ({ contestId, groupId }) => {
    try {
        let cond = {}
        cond.contest = [contestId]
        cond.group = [groupId]
        let chatObj = await chatgrpModelMethods.getAllgrpChats(cond);
        return { ...httpStatus.FETCHED, chatObj };
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error);
    }
};

module.exports = chatgrpController;

