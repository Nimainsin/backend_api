
const queryController = {};
const errorHandler = require("../utils/errorHandler")
const httpStatus = require("../constants/httpStatus")
const mailer = require("../constants/mailer")
const config = require("../config")
const queryModelMethod = require("../models/query")
const request = require("request")

queryController.insertQuery = async ({ queryObj, captcha, remoteAddress }) => {
    let queryObjEdited = queryObj
    try {
        if (
            captcha === undefined
            || captcha === '' ||
            captcha === null
        ) {

            return { ...httpStatus.BAD_REQUEST, message: "Please Select Capcha" }

        }

        // secret Key
        const secretKey = config.SECRET_KEY
        // verifyUrl
        const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${captcha}&remoteip=${remoteAddress}`

        // make request  to verify url
        request(verifyUrl, (err, response, body) => {
            body = JSON.parse(body)

            // if not success
            if (body.success !== undefined && !body.success) {
                return { ...httpStatus.BAD_REQUEST, message: "Failed Capcha Verification" }
            }
        })
        console.log("Capcha Passed")
        let message = ''
        let queryObj = await queryModelMethod.insertQuery(queryObjEdited)

        const { firstName, lastName, email, query, company } = queryObjEdited

        message += (
            '<div >' +
            '<p >' + '<b>Name :</b>' + firstName + ' ' + lastName + '</p>' +
            '<p >' + '<b>Email :</b>' + email + '</p>' +
            '<p >' + '<b>Company :</b>' + company + '</p>' +
            '<p >' + '<b>Query :</b>' + query + '</p>' +

            '</div>'
        );

        let mailOpts = {
            from: config.EMAIL_USER,
            to: config.EMAIL_USER,
            subject: mailer.mailSubject.CONTACT.subject,
            html: message
        }

        mailer.smtpTrans.sendMail(mailOpts, (error, response) => {
            if (error) {
                console.log(error);
                return httpStatus.INTERNAL_SERVER_ERROR// Show a page indicating failure
            }
            else {
                console.log(response); // Show a page indicating success
                return { ...httpStatus.OK }
            }
        })
        // console.log(queryObjEdited)
        return { ...httpStatus.OK, queryObj }
    } catch (error) {
        console.log(error);
        return errorHandler.errorHandlerMain(error)
    }
}

module.exports = queryController;