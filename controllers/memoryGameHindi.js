const memoryGameHindiController = {}
const httpStatus = require('../constants/httpStatus')
const errorHandler = require('../utils/errorHandler')
const memoryGameModelMethods = require('../models/memoryGameHindi')
const handlePagination = require('../helper/pagination')

memoryGameHindiController.insertMemoryGame = async memory => {
  try {
    let memoryGameObj = await memoryGameModelMethods.insertMemoryGame(memory)
    return { ...httpStatus.OK, memoryGameObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

memoryGameHindiController.editMemoryGame = async (memory, _id) => {
  try {
    if (memory.question && memory.question.length !== 4)
      return {
        ...httpStatus.BAD_REQUEST,
        memory: 'Questions Should be a set of 4 questions!',
      }
    let memoryGameObj = await memoryGameModelMethods.editMemoryGame(memory, _id)
    return { ...httpStatus.OK, memoryGameObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

memoryGameHindiController.getAllMemoryGames = async ({ page, perPage }) => {
  try {
    let memoryGameObj = await memoryGameModelMethods.getAllMemoryGames()
    let pagination = await handlePagination(memoryGameObj, page, perPage)
    return { ...httpStatus.OK, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

memoryGameHindiController.getMemoryGame = async _id => {
  try {
    let memoryGameObj = await memoryGameModelMethods.getMemoryGame(_id)
    return { ...httpStatus.OK, memoryGameObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

memoryGameHindiController.deleteMemoryGame = async _id => {
  try {
    let memoryGameObj = await memoryGameModelMethods.deleteMemoryGame(_id)
    return { ...httpStatus.OK, memoryGameObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}
module.exports = memoryGameHindiController
