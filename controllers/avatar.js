const avatarController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const avatarModelMethod = require("../models/avatar");

avatarController.insertAvatar = async (avatar) => {
  try {
    let avatarCount = await avatarModelMethod.avatarCount()
    avatar.avatarId = avatarCount + 1
    let avatarObj = await avatarModelMethod.createAvatar(avatar);
    return { ...httpStatus.OK, avatarObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

avatarController.editAvatar = async (avatar, _id) => {
  try {
    let avatarObj = await avatarModelMethod.editAvatar(avatar, _id);
    return { ...httpStatus.OK, avatarObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

avatarController.getAvatar = async (_id) => {
  try {
    let avatarObj = await avatarModelMethod.getAvatar(_id);
    return { ...httpStatus.OK, avatarObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

avatarController.getAllAvatar = async () => {
  try {
    let avatarObj = await avatarModelMethod.getAllAvatars();
    return { ...httpStatus.OK, avatarObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

avatarController.deleteAvatar = async (_id) => {
  try {
    let avatarObj = await avatarModelMethod.deleteAvatar(_id);
    return { ...httpStatus.OK, avatarObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};
module.exports = avatarController;
