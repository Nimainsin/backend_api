const orgModuleController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const OrgModuleModelMethod = require("../models/orgModule");
const handlePagination = require("../helper/pagination");

orgModuleController.insertOrgModule = async (OrgModule) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.insertOrgModule(OrgModule);
    return { ...httpStatus.OK, orgModuleObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgModuleController.editOrgModule = async (OrgModule, _id) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.editOrgModule(OrgModule, _id);
    return { ...httpStatus.OK, orgModuleObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgModuleController.getOrgModule = async (_id) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.getOrgModule(_id);
    return { ...httpStatus.OK, orgModuleObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgModuleController.getAllOrgModule = async ({ page, perPage }) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.getAllOrgModules();
    let pagination = await handlePagination(orgModuleObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

orgModuleController.deleteOrgModule = async (_id) => {
  try {
    let orgModuleObj = await OrgModuleModelMethod.deleteOrgModule(_id);
    return { ...httpStatus.OK, orgModuleObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = orgModuleController;
