const winnerController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const WinnerModelMethod = require("../models/winner");
const contestModelMethods = require("../models/gameContest");
const userModelMethods = require("../models/user");
const handlePagination = require("../helper/pagination");

winnerController.insertWinner = async (winner) => {
  try {
    let winnerObj = await WinnerModelMethod.insertWinner(winner);
    return { ...httpStatus.OK, winnerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.editWinner = async (winner, _id) => {
  try {
    let winnerObj = await WinnerModelMethod.editWinner(winner, _id);
    return { ...httpStatus.OK, winnerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.getWinner = async (_id) => {
  try {
    let winnerObj = await WinnerModelMethod.findWinner(_id);
    return { ...httpStatus.OK, winnerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.getAllWinner = async (id) => {
  try {
    let winnerObj = await WinnerModelMethod.findAllWinners(id);
    console.log(winnerObj);
    let contestInfo = await contestModelMethods.findContest(id);
    let groupArr = contestInfo.group;
    let winnerFreq = {};
    for (let winner of winnerObj) {
      for (let group of groupArr) {
        for (let user of group.user) {
          // console.log(user);
          if (winner.userId === user.id) {
            console.log(winner.userId === user.id, winner.userId);
            if (winnerFreq[group.name]) {
              winnerFreq[group.name] = winnerFreq[group.name] + 1;
            } else {
              winnerFreq[group.name] = 1;
            }
            break;
          }
        }
      }
    }
    console.log(winnerFreq);
    let winnerGroupList = Object.keys(winnerFreq).sort(function (a, b) {
      return winnerFreq[b] - winnerFreq[a];
    });
    console.log(winnerGroupList);
    let winnerGroupRank = [];
    for (let [i, grp] of winnerGroupList.entries()) {
      winnerGroupRank.push({ name: grp, position: i + 1 });
    }
    let playerRank = [];
    for (let [i, winner] of winnerObj.entries()) {
      let user = await userModelMethods.findUser(winner.userId);
      let userObj = { position: i + 1, ...user._doc };
      playerRank.push(userObj);
    }
    return { ...httpStatus.OK, result: { winnerGroupRank, playerRank } };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.getAllWinnerGroupRank = async ({
  contestId,
  page,
  perPage,
}) => {
  try {
    let winnerObj = await WinnerModelMethod.findAllWinners(contestId);
    console.log(winnerObj);
    let contestInfo = await contestModelMethods.findContest(contestId);
    let groupArr = contestInfo.group;
    let winnerFreq = {};
    for (let winner of winnerObj) {
      for (let group of groupArr) {
        for (let user of group.user) {
          // console.log(user);
          if (winner.userId === user.id) {
            console.log(winner.userId === user.id, winner.userId);
            if (winnerFreq[group.name]) {
              winnerFreq[group.name] = winnerFreq[group.name] + 1;
            } else {
              winnerFreq[group.name] = 1;
            }
            break;
          }
        }
      }
    }
    console.log(winnerFreq);
    let winnerGroupList = Object.keys(winnerFreq).sort(function (a, b) {
      return winnerFreq[b] - winnerFreq[a];
    });
    console.log(winnerGroupList);
    let winnerGroupRank = [];
    for (let [i, grp] of winnerGroupList.entries()) {
      winnerGroupRank.push({ name: grp, position: i + 1 });
    }
    let playerRank = [];
    for (let [i, winner] of winnerObj.entries()) {
      let user = await userModelMethods.findUser(winner.userId);
      let userObj = { position: i + 1, ...user._doc };
      playerRank.push(userObj);
    }
    // return { ...httpStatus.OK, result: { winnerGroupRank, playerRank } };
    let pagination = await handlePagination(winnerGroupRank, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.getAllPlayerRank = async ({ contestId, page, perPage }) => {
  try {
    let winnerObj = await WinnerModelMethod.findAllWinners(contestId);
    console.log(winnerObj);
    let contestInfo = await contestModelMethods.findContest(contestId);
    let groupArr = contestInfo.group;
    let winnerFreq = {};
    for (let winner of winnerObj) {
      for (let group of groupArr) {
        for (let user of group.user) {
          // console.log(user);
          if (winner.userId === user.id) {
            console.log(winner.userId === user.id, winner.userId);
            if (winnerFreq[group.name]) {
              winnerFreq[group.name] = winnerFreq[group.name] + 1;
            } else {
              winnerFreq[group.name] = 1;
            }
            break;
          }
        }
      }
    }
    console.log(winnerFreq);
    let winnerGroupList = Object.keys(winnerFreq).sort(function (a, b) {
      return winnerFreq[b] - winnerFreq[a];
    });
    console.log(winnerGroupList);
    let winnerGroupRank = [];
    for (let [i, grp] of winnerGroupList.entries()) {
      winnerGroupRank.push({ name: grp, position: i + 1 });
    }
    let playerRank = [];
    for (let [i, winner] of winnerObj.entries()) {
      let user = await userModelMethods.findUser(winner.userId);
      let userObj = { position: i + 1, ...user._doc };
      playerRank.push(userObj);
    }
    // return { ...httpStatus.OK, result: { winnerGroupRank, playerRank } };
    let pagination = await handlePagination(playerRank, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

winnerController.deleteWinner = async (_id) => {
  try {
    let winnerObj = await WinnerModelMethod.removeWinner(_id);
    return { ...httpStatus.OK, winnerObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = winnerController;
