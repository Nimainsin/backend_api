/* eslint-disable no-unused-vars */
const limitController = {}
const errorHandler = require('../utils/errorHandler')
const httpStatus = require('../constants/httpStatus')
const LimitModelMethod = require('../models/limit')

limitController.insertLimit = async limit => {
  try {
    let limitObj = await LimitModelMethod.createLimit(limit)
    return { ...httpStatus.OK, limitObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

limitController.editLimit = async (limit, _id) => {
  try {
    let limitObj = await LimitModelMethod.editLimit(limit, _id)
    return { ...httpStatus.OK, limitObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

limitController.getLimit = async _id => {
  try {
    let limitObj = await LimitModelMethod.getLimit(_id)
    return { ...httpStatus.OK, limitObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

limitController.getAllLimit = async ({ page, perPage }) => {
  try {
    let limitObj = await LimitModelMethod.getAllLimits()

    return { ...httpStatus.OK, limitObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

limitController.deleteLimit = async _id => {
  try {
    let limitObj = await LimitModelMethod.deleteLimit(_id)
    return { ...httpStatus.OK, limitObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}
module.exports = limitController
