const questionBankController = {}
const httpStatus = require('../constants/customHttpStatus')
const errorHandler = require('../utils/errorHandler')
const questionBankmodelMethod = require('../models/questionBank')
const answerModelMethod = require('../models/answer')
const handlePagination = require('../helper/pagination')
const fileFolderHandler = require('../utils/fileFolderHandler')
const path = require('path')
const XLSX = require('xlsx')

questionBankController.insertQuestion = async question => {
  try {
    question.questionCategory = [question.questionCategoryId]
    let questionObj = await questionBankmodelMethod.insertQuestion(question)
    question.question = [questionObj.id]
    await answerModelMethod.insertAnswer(question)
    return { ...httpStatus.CREATED, questionObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.insertBulkQuestion = async ({ files }) => {
  try {
    let file = files.file
    console.log(file)
    // create directories if doesn't exist
    await fileFolderHandler.createFolder('questionfile')

    if (file) {
      let fileName = path.join(
        'questionfile',
        `questionfile_${Date.now()}${path.extname(file.name)}`
      )
      let newPath = path.join(__dirname, '..', 'public', fileName)
      //let oldPath = file.path

      //let fileInfo = await fileHandler.filePathHandler(newPath, fileName, oldPath)
      // CureTeam.image = fileInfo.fileName;
      const workbook = XLSX.readFile(newPath)
      let sheetNameList = workbook.SheetNames
      let questionArrObj = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]])
      let questionObj = await questionBankmodelMethod.insertBulkQuestion(questionArrObj)
      return { ...httpStatus.CREATED, questionObj }
    }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.editQuestion = async (question, _id) => {
  try {
    if (question.questionCategoryId) question.questionCategory = [question.questionCategoryId]
    let questionObj = await questionBankmodelMethod.editQuestion(question, _id)
    console.log(questionObj)
    let answerObj = await answerModelMethod.editAnswer(question, [questionObj.id])
    console.log(answerObj)
    return { ...httpStatus.UPDATED, questionObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.getAllQuestions = async ({ page = 1, perPage = 4 }) => {
  try {
    let questionBankObj = await questionBankmodelMethod.getAllQuestions()
    let pagination = await handlePagination(questionBankObj, page, perPage)
    return { ...httpStatus.FETCHED, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.getAllQuestionsInHindi = async ({ page = 1, perPage = 4 }) => {
  try {
    let questionBankObj = await questionBankmodelMethod.getAllQuestions()
    // for (let con of questionBankObj) {
    //   const questionTitle = await translate(con.questionTitle, { from: 'en', to: 'hi' })
    //   console.log(questionTitle.text);
    //   con.questionTitle = questionTitle.text

    //   const option1 = await translate(con.option1, { from: 'en', to: 'hi' })
    //   console.log(option1.text);
    //   con.option1 = option1.text

    //   const option2 = await translate(con.option2, { from: 'en', to: 'hi' })
    //   console.log(option2.text);
    //   con.option1 = option2.text

    //   const option3 = await translate(con.option3, { from: 'en', to: 'hi' })
    //   console.log(option3.text);
    //   con.option1 = option3.text

    //   const option4 = await translate(con.option4, { from: 'en', to: 'hi' })
    //   console.log(option4.text);
    //   con.option1 = option4.text

    //   // hindiObj.push(con)
    // }
    let pagination = await handlePagination(questionBankObj, page, perPage)
    return { ...httpStatus.FETCHED, ...pagination }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.getQuestion = async _id => {
  try {
    let questionObj = await questionBankmodelMethod.getQuestion(_id)
    return { ...httpStatus.FETCHED, questionObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.getQuestionsWithAnswer = async () => {
  try {
    let questionObj = await answerModelMethod.findAllAnswers()
    return { ...httpStatus.FETCHED, questionObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.removeQuestion = async _id => {
  try {
    let questionObj = await questionBankmodelMethod.deleteQuestion(_id)
    console.log(questionObj)
    let answerObj = await answerModelMethod.removeAnswer({ question: _id })
    console.log(answerObj)
    return { ...httpStatus.DELETED, message: 'Question Deleted Successfully' }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

questionBankController.verifyAnswer = async ({ questionId, answer }) => {
  try {
    let answerObj = await answerModelMethod.checkAnswer({
      question: [questionId],
      answer,
    })
    let status = false
    if (answerObj) status = true
    return { ...httpStatus.FETCHED, answer: status }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = questionBankController
