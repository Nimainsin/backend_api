const obstacleController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const fileHandler = require("../utils/fileHandler");
const obstacleModelMethod = require("../models/obstacle");
const path = require("path");
const fs = require("fs");
const handlePagination = require("../helper/pagination");

obstacleController.insertObstacle = async ({ fields, files }) => {
  try {
    let Obstacle = fields;
    let file = files.image;

    // create directories if doesn't exist

    let dir = path.join(__dirname, "..", "public");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    dir = path.join(__dirname, "..", "public", "ObstacleImages");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    if (file) {
      let fileName = path.join(
        "ObstacleImages",
        `ObstacleImage_${Date.now()}${path.extname(file.name)}`
      );
      let newPath = path.join(__dirname, "..", "public", fileName);
      let oldPath = file.path;

      let fileInfo = await fileHandler.filePathHandler(
        newPath,
        fileName,
        oldPath
      );
      Obstacle.image = fileInfo.fileName;
    }

    let obstacleObj = await obstacleModelMethod.createObstacle(Obstacle);
    return { ...httpStatus.OK, obstacleObj };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

obstacleController.editObstacle = async ({ fields, files, _id }) => {
  try {
    let Obstacle = fields;
    let file = files.image;

    // create directories if doesn't exist
    let newFile = false;
    let dir = path.join(__dirname, "..", "public");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    dir = path.join(__dirname, "..", "public", "ObstacleImages");
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    if (file) {
      let fileName = path.join(
        "ObstacleImages",
        `ObstacleImage_${Date.now()}${path.extname(file.name)}`
      );
      let newPath = path.join(__dirname, "..", "public", fileName);
      let oldPath = file.path;

      let fileInfo = await fileHandler.filePathHandler(
        newPath,
        fileName,
        oldPath
      );
      Obstacle.image = fileInfo.fileName;
      newFile = true;
    }
    let existedObstacle = await obstacleModelMethod.getObstacle(_id);

    if (existedObstacle && existedObstacle.image && newFile) {
      let filePathToDelete = path.join(
        __dirname,
        "..",
        "public",
        existedObstacle.image
      );

      if (fs.existsSync(filePathToDelete))
        await fileHandler.unlinkFileHandler(filePathToDelete);
    }

    let obstacleObj = await obstacleModelMethod.editObstacle(Obstacle, _id);
    return { ...httpStatus.OK, obstacleObj };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

obstacleController.getObstacle = async (_id) => {
  try {
    let obstacleObj = await obstacleModelMethod.getObstacle(_id);
    return { ...httpStatus.OK, obstacleObj };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

obstacleController.getAllObstacle = async ({ page, perPage }) => {
  try {
    let obstacleObj = await obstacleModelMethod.getAllObstacles();
    let pagination = await handlePagination(obstacleObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};

obstacleController.deleteObstacle = async (_id) => {
  try {
    let obstacleObj = await obstacleModelMethod.deleteObstacle(_id);
    return { ...httpStatus.OK, obstacleObj };
  } catch (error) {
    return errorHandler.errorHandlerMain(error);
  }
};
module.exports = obstacleController;
