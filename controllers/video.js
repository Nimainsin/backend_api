/* eslint-disable no-unused-vars */
const videoController = {}
const errorHandler = require('../utils/errorHandler')
const httpStatus = require('../constants/httpStatus')
const videoModelMethod = require('../models/video')
const config = require('../config')
const azureStorageHandler = require('../utils/azureStorageHandler')

videoController.insertVideo = async ({ fields, files }) => {
  try {
    let video = fields
    let file = files.file
    if (file) {
      let urlLink = await azureStorageHandler.azureStorageHandler(file)
      if (urlLink) video.urlLink = urlLink
    }

    let VideoObj = await videoModelMethod.insertVideo(video)
    return { ...httpStatus.OK, VideoObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

videoController.editVideo = async ({ fields, files, id }) => {
  try {
    let video = fields
    let file = files.file

    if (file) {
      let urlLink = await azureStorageHandler.azureStorageHandler(file)
      if (urlLink) video.urlLink = urlLink
    }
    let videoObj = await videoModelMethod.editVideo(video, id)

    return { ...httpStatus.OK, videoObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

videoController.getVideo = async _id => {
  try {
    let VideoObj = await videoModelMethod.findVideo(_id)
    return { ...httpStatus.OK, VideoObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

videoController.getAllVideo = async ({ page, perPage }) => {
  try {
    let dataList = await videoModelMethod.findAllVideos()
    if (page !== undefined && perPage !== undefined) {
      let totalPages = Math.ceil(dataList.length / perPage)
      let calculatedPage = (page - 1) * perPage
      let calculatedPerPage = page * perPage
      return {
        ...httpStatus.OK,
        data: dataList.slice(calculatedPage, calculatedPerPage),
        totalPages,
        allData: dataList,
        totalRecords: dataList.length,
      }
    } else {
      return {
        ...httpStatus.OK,
        data: dataList.slice(0, 4),
        allData: dataList,
        totalPages: Math.ceil(dataList.length / 4),
        totalRecords: dataList.length,
      }
    }
  } catch (error) {
    return errorHandler.errorHandlerMain(error)
  }
}

videoController.deleteVideo = async _id => {
  try {
    let VideoObj = await videoModelMethod.removeVideo(_id)
    return { ...httpStatus.OK, VideoObj }
  } catch (error) {
    console.log(error)
    return errorHandler.errorHandlerMain(error)
  }
}

module.exports = videoController
