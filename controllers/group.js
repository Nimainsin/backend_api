const groupController = {};
const errorHandler = require("../utils/errorHandler");
const httpStatus = require("../constants/httpStatus");
const groupModelMethods = require("../models/group");
const handlePagination = require("../helper/pagination");

groupController.insertGroup = async (group) => {
  try {
    let existedGroup = await groupModelMethods.findGroupByName(group.name);
    if (existedGroup)
      return {
        ...httpStatus.BAD_REQUEST,
        message: "Group Name Already Taken!",
      };
    group.user = group.userIds;
    group.village = [group.villageId];
    let groupObj = await groupModelMethods.insertGroup(group);
    return { ...httpStatus.OK, groupObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

groupController.editGroup = async (group, id) => {
  try {
    let existedGroup = await groupModelMethods.findGroup(id);
    if (!existedGroup)
      return {
        ...httpStatus.BAD_REQUEST,
        message: "The Record you want to edit doesn't exist",
      };
    if (group.userIds) group.user = group.userIds;

    if (group.villageId) group.village = [group.villageId];

    let groupObj = await groupModelMethods.editGroup(group, id);
    return { ...httpStatus.OK, message: "Group Edited Successfully", groupObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

groupController.getGroup = async (_id) => {
  try {
    let groupObj = await groupModelMethods.findGroup(_id);
    return { ...httpStatus.OK, groupObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

groupController.getAllGroup = async ({ page, perPage }) => {
  try {
    let groupObj = await groupModelMethods.findAllGroups();
    let pagination = await handlePagination(groupObj, page, perPage);
    return { ...httpStatus.OK, ...pagination };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

groupController.deleteGroup = async (_id) => {
  try {
    let groupObj = await groupModelMethods.removeGroup(_id);
    return { ...httpStatus.OK, groupObj };
  } catch (error) {
    console.log(error);
    return errorHandler.errorHandlerMain(error);
  }
};

module.exports = groupController;
