const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("../routes");
const middleware = require("../utils/middleware");
const static = require("kvell-scripts").static;
const path = require("path");
const swaggerUi = require("swagger-ui-express"),
  swaggerDocument = require("../swagger.json");

/**
 *
 * @param {import ("kvell-scripts").KvellAppObject} app
 */
//sequelize.sync();

const globalMiddlewares = (app) => {
  app.use(middleware.formidable, bodyParser.json(), bodyParser.urlencoded({ extended: true }));
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  app.use("/static", static(path.join(__dirname, "..", "public")));

  app.use("/query/", routes.query)
  app.use("/vacancy/", routes.vacancy)
  app.use("/user/", routes.user)
  app.use("/login/", routes.login)
  app.use("/signUp/", routes.signUp)
  app.use("/video/", routes.video)
  app.use("/questionCategory/", routes.questionCategory)
  app.use("/answer/", routes.answer)
  app.use("/village/", routes.village)
  app.use("/state/", routes.state)
  app.use("/group/", routes.group)
  app.use("/district/", routes.district)
  app.use("/block/", routes.block)
  app.use("/gameContest/", routes.gameContest)
  app.use("/obstacle/", routes.obstacle)
  app.use("/gameLevel/", routes.gameLevel)
  app.use("/questionBank/", routes.questionBank)
  app.use("/memoryGame/", routes.memoryGame)
  app.use("/reward/", routes.reward)
  app.use("/orgUserRole/", routes.orgUserRole)
  app.use("/orgModule/", routes.orgModule)
  app.use("/modulePriviledge/", routes.modulePriviledge)
  app.use("/orgUser/", routes.orgUser)
  app.use("/avatar/", routes.avatar)
  app.use("/otp/", routes.otp)
  app.use("/winner/", routes.winner)
  app.use("/limit/", routes.limit)
  app.use("/questionBankHindi/", routes.questionBankHindi)
  app.use("/memoryGameHindi/", routes.memoryGameHindi)
  app.use("/rank/", routes.rank)
  app.use("/count/", routes.count)
  app.use("/chat/", routes.chat)
  app.use("/chatgrp/", routes.chatgrp)
  app.use("/orgModule2/", routes.orgModule2)

  app.use(middleware.auth);

  //private apis

  app.use(cors());
};

module.exports = globalMiddlewares;
