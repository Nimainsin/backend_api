const handlePagination = (dataList, page=1, perPage=5) => {
  if (page !== undefined && perPage !== undefined) {
    let totalPages = Math.ceil(dataList.length / perPage);
    let calculatedPage = (page - 1) * perPage;
    let calculatedPerPage = page * perPage;
    return {
      allData: dataList,
      data: dataList.slice(calculatedPage, calculatedPerPage),
      totalPages,
      totalRecords: dataList.length,
    };
  } else {
    return {
      allData: dataList,
      data: dataList.slice(0, 4),
      totalPages: Math.ceil(dataList.length / 4),
      totalRecords: dataList.length,
    };
  }
};

module.exports = handlePagination;
