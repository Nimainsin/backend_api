const mongoose = require("kvell-db-plugin-mongoose").dbLib;

const empFilesSchema = require("../models/empFiles/empFilesModel");
const empSchema = require("../models/emp/empModel");
const joiningSchema = require("../models/joining/joiningModel");
const joiningEducationSchema = require("../models/joiningEducation/joiningEducationModel");
const joinPersonalSchema = require("../models/joinPersonal/joinPersonalModel");
const joinExperienceSchema = require("../models/joinExperience/joinExperienceModel");
const joinMiscSchema = require("../models/joinMisc/joinMiscModel");
const bankDetailsSchema = require("../models/bankDetails/bankDetailsModel");
const nominationSchema = require("../models/nomination/nominationModel");
const nomPartASchema = require("../models/nomPartA/nomPartAModel");
const nomPartBSchema = require("../models/nomPartB/nomPartBModel");
const nomWidowPensionSchema = require("../models/nomWidowPension/nomWidowPensionModel");
const nomEmployerCertSchema = require("../models/nomEmployerCert/nomEmployerCertModel");
const nomMiscSchema = require("../models/nomMisc/nomMiscModel");
const form11Schema = require("../models/form11/form11Model");
const gratuitySchema = require("../models/gratuity/gratuityModel");
const gratuityNomineeSchema = require("../models/gratuityNominee/gratuityNomineeModel");
const gratuityWitnessSchema = require("../models/gratuityWitness/gratuityWitnessModel");
const familySchema = require("../models/family/familyModel");
const familyMemberSchema = require("../models/familyMember/familyMemberModel");

const models = {
  Emp: mongoose.model("Emp", empSchema),
  EmpFile: mongoose.model("EmpFile", empFilesSchema),
  Joining: mongoose.model("Joining", joiningSchema),

  JoiningEducation: mongoose.model("JoiningEducation", joiningEducationSchema),
  JoinPersonal: mongoose.model("JoinPersonal", joinPersonalSchema),
  JoinExperience: mongoose.model("JoinExperience", joinExperienceSchema),

  JoinMisc: mongoose.model("JoinMisc", joinMiscSchema),
  BankDetails: mongoose.model("BankDetails", bankDetailsSchema),
  Nomination: mongoose.model("Nomination", nominationSchema),

  NomPartASchema: mongoose.model("NomPartASchema", nomPartASchema),
  NomPartB: mongoose.model("NomPartB", nomPartBSchema),
  NomWidowPension: mongoose.model("NomWidowPension", nomWidowPensionSchema),

  NomEmployerCert: mongoose.model("NomEmployerCert", nomEmployerCertSchema),
  NomMisc: mongoose.model("NomMisc", nomMiscSchema),
  Form11: mongoose.model("Form11", form11Schema),
  Gratuity: mongoose.model("Gratuity", gratuitySchema),
  GratuityNominee: mongoose.model("GratuityNominee", gratuityNomineeSchema),
  GratuityWitness: mongoose.model("GratuityWitness", gratuityWitnessSchema),
  Family: mongoose.model("Family", familySchema),
  FamilyMember: mongoose.model("FamilyMember", familyMemberSchema),
};

const createCollections = async () => {
  // explicitly create each collection
  // for Mongoose multi-document write support which is needed for transactions

  //    mongoose.connection.db.listCollections().toArray(function (err, names) {
  //         console.log("names ", names); // [{ name: 'dbname.myCollection' }]
  //         // module.exports.Collection = names;
  //     });

  let collectionCount = await mongoose.connection.db
    .listCollections()
    .toArray();
  collectionCount = collectionCount.length;
  if (collectionCount === 0) {
    Object.values(models).forEach((model, i) => {
      console.log("created collection", i + 1, model);
      try {
        model.createCollection();
      } catch (error) {
        console.log("error", error);
      }
    });
  }
};

createCollections();

module.exports = { models };
