const dateTimeHandler = {};
const moment = require("moment");

dateTimeHandler.convertTimeStampToDateTime = (timeStamp) =>
  moment(timeStamp).format("YYYY-MM-DD HH:mm:ss");
// moment(timeStamp).format("YYYY-MM-DD");

module.exports = dateTimeHandler;
