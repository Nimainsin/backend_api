// const Redis = require("ioredis");
// const redis = new Redis();
// const redisMethods = {};

// /**
//  * Store key-value pair in redis.
//  *
//  * @param {string}  key
//  * @param {string} value
//  * @param {string} exp
//  * @param {number} time
//  *
//  */

// redisMethods.setRedis = (key, value, exp, time) => redis.set(key, value, exp, time);

// /**
//  * Fetch value of stored key in redis.
//  *
//  * @param {string} key
//  *
//  */

// redisMethods.getRedis = (key) => redis.get(key);

// /**
//  * Delete key from redis.
//  *
//  * @param {string} key
//  *
//  */

// redisMethods.deleteRedis = (key) => redis.del(key);

// /**
//  * Insert hashed values in redis.
//  *
//  * @param {string} key
//  * @param {string} childKey
//  *  @param {string} payload
//  */

// redisMethods.insertInRedisHashed = (key, childKey, payload) => redis.hset(key, childKey, payload);

// /**
//  * Fetch hashed values list in redis.
//  *
//  * @param {string} key
//  */

// redisMethods.getRedisHashedList = (key) => redis.hgetall(key);

// /**
//  * Fetch hashed key by child key in redis.
//  *
//  * @param {string} key
//  * @param {string} childKey
//  */

// redisMethods.getHashedRedisByChildKey = (key, childKey) => redis.hget(key, childKey);

// /**
//  * Delete hashed key in redis.
//  *
//  * @param {string} key
//  * @param {string} childKey
//  */

// redisMethods.deleteHashedRedisByChildKey = (key, childKey) => redis.hdel(key, childKey);
// module.exports = redisMethods;
