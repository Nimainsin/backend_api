

const config = require("../config");
const { BlobServiceClient, StorageSharedKeyCredential, newPipeline } = require('@azure/storage-blob');
const fs = require('fs');

exports.azureStorageHandler = async (file) => {
    try {
        console.log("in azure");
        const sharedKeyCredential = new StorageSharedKeyCredential(config.AZURE_STORAGE_NAME, config.AZURE_KEY);
        console.log(".........................")
        const pipeline = newPipeline(sharedKeyCredential);
        console.log(".........................")
        const blobServiceClient = new BlobServiceClient(
            `https://${config.AZURE_STORAGE_NAME}.blob.core.windows.net`,
            pipeline
        );


        if (file) {
            console.log(".............file............")
            let blobName = getBlobName(file.name);
            const containerClient = blobServiceClient.getContainerClient(config.AZURE_CONTAINER);

            const blockBlobClient = containerClient.getBlockBlobClient(blobName)


            await blockBlobClient.uploadStream(fs.createReadStream(file.path));
            console.log("...........upload..file............")
            console.log("Blob was uploaded successfully. requestId: ");
            console.log("Blob URL: ", blockBlobClient.url)


            return blockBlobClient.url
        }

        return false

    } catch (error) {
        console.log(error)
        return false
    }
}

const getBlobName = originalName => {
    const identifier = Math.random().toString().replace(/0\./, '');
    return `${identifier}-${originalName}`;
};

