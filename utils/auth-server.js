const axois = require("axios").default;
const BASE_URL = process.env.AUTH_URL;

/**
 * @typedef {Object} payload
 */

/**
 * Generate Identity for particular user
 * @param {payload} payload
 * @param {string} appId
 * @param {boolean} [isEmail]
 */

exports.generate = async (appId, payload, isEmail = false) =>
	await axois.post(`${BASE_URL}/gen`, { appId, payload, isEmail });

/**
 * Verify Identity for particular user
 * @param {string} token
 * @param {boolean} [isEmail]
 */

exports.verify = async (token, isEmail = false) =>
	await axois.post(`${BASE_URL}/verify`, { token, isEmail });

/**
 * log-out a user
 * @param {string} email email of the user to log-out
 */

exports.logout = async (email, ip) => {
	const appId = "dl";
	const response = await axois.get(
		`${BASE_URL}/manage/logout?email=${email}&appId=${appId}&ip=${ip.replace("::ffff:", "")}`
	);
	return response.data;
};
