const nominationUtils = {}

nominationUtils.createNominationInput = (emp, nomPartAObj, nomPartBObj, nomWidowPenObj, nomEmpCertObj, nomMiscObj) => {
    let nomPartA = nomPartAObj.map(nom => nom._id)
    let nomPartB = nomPartBObj.map(nom => nom._id)
    let nomWidowPension = nomWidowPenObj.map(nom => nom._id)
    let nomEmployerCert = [nomEmpCertObj[0]._id]
    let nomMisc = [nomMiscObj[0]._id]

    return {
        nomName: emp.nomName,
        nomFatherName: emp.nomFatherName,
        nomDob: emp.nomDob,
        nomSex: emp.nomSex,
        nomMartial: emp.nomMartial,
        nomAcc: emp.nomAcc,
        nomPermAdd: emp.permAddress,
        nomPresAdd: emp.presAddress,
        nomPartA,
        nomPartB,
        nomWidowPension,
        nomEmployerCert,
        nomMisc
    }
}

nominationUtils.createNomPartAInput = (emp,) => {
    return [{

        nomNomineeName: emp.nominee1,
        nomNomineeAdd: emp.address1,
        nomNomineeRelation: emp.memberReletionship1,
        nomNomineeDob: emp.nomDob1,
        nomNomineeAmount: emp.nomAmount1,
        nomNomineeMinor: emp.nomMinor1,

    },
    {

        nomNomineeName: emp.nominee2,
        nomNomineeAdd: emp.address2,
        nomNomineeRelation: emp.memberReletionship2,
        nomNomineeDob: emp.nomDob2,
        nomNomineeAmount: emp.nomAmount2,
        nomNomineeMinor: emp.nomMinor2,

    },
    {

        nomNomineeName: emp.nominee3,
        nomNomineeAdd: emp.address3,
        nomNomineeRelation: emp.memberReletionship3,
        nomNomineeDob: emp.nomDob3,
        nomNomineeAmount: emp.nomAmount3,
        nomNomineeMinor: emp.nomMinor3,

    },
    {

        nomNomineeName: emp.nominee4,
        nomNomineeAdd: emp.address4,
        nomNomineeRelation: emp.memberReletionship4,
        nomNomineeDob: emp.nomDob4,
        nomNomineeAmount: emp.nomAmount4,
        nomNomineeMinor: emp.nomMinor4,

    },
    {

        nomNomineeName: emp.nominee5,
        nomNomineeAdd: emp.address5,
        nomNomineeRelation: emp.memberReletionship5,
        nomNomineeDob: emp.nomDob5,
        nomNomineeAmount: emp.nomAmount5,
        nomNomineeMinor: emp.nomMinor5,

    }

    ]
}

nominationUtils.createNomPartBInput = (emp) => {
    return [{
        nomPensionHolderName: emp.pensionHolder1,
        nomPensionHolderAdd: emp.pensionHolderAdd1,
        nomPensionHolderDob: emp.pensionHolderDob1,
        nomPensionHolderRelation: emp.pensionHolderRelationship1,

    }, {
        nomPensionHolderName: emp.pensionHolder2,
        nomPensionHolderAdd: emp.pensionHolderAdd2,
        nomPensionHolderDob: emp.pensionHolderDob2,
        nomPensionHolderRelation: emp.pensionHolderRelationship2,

    },
    {
        nomPensionHolderName: emp.pensionHolder3,
        nomPensionHolderAdd: emp.pensionHolderAdd3,
        nomPensionHolderDob: emp.pensionHolderDob3,
        nomPensionHolderRelation: emp.pensionHolderRelationship3,

    }

    ]
}

nominationUtils.createNomWidowPenInput = (emp) => {
    return [{
        nomWidowPensionHolderName: emp.widowPensionHolderName1,
        nomWidowPensionHolderDob: emp.widowPensionHolderdob1,
        nomWidowPensionHolderRelation: emp.widowPensionHolderRelationship1,

    },
    {
        nomWidowPensionHolderName: emp.widowPensionHolderName2,
        nomWidowPensionHolderDob: emp.widowPensionHolderdob2,
        nomWidowPensionHolderRelation: emp.widowPensionHolderRelationship2,

    },
    {
        nomWidowPensionHolderName: emp.widowPensionHolderName3,
        nomWidowPensionHolderDob: emp.widowPensionHolderdob3,
        nomWidowPensionHolderRelation: emp.widowPensionHolderRelationship3,

    }

    ]
}

nominationUtils.createNomEmpCertInput = (emp) => {
    return {
        certiEmpName: emp.certiEmpName,
        employerSign: emp.employerSign,
        factoryInfo: emp.factoryInfo,
        factoryPlace: emp.factoryPlace,
        factoryDesignation: emp.factoryDesignation,
        certiDate: emp.certiDate,

    }
}

nominationUtils.createNomMiscInput = (emp) => {
    return {
        nomPartAParentDependency: emp.parentDependency,
        nomPartANomineeSign: emp.nomSign,
        nomPartANoFamily: emp.noFamily,
        nomPartBNoFamily: emp.nomPartBNoFamily,
        nomPartBWidowPension: emp.nomPartBWidowPension,
        nomPartBSign: emp.nomPartBSign,
        nomPartBDate: emp.nomPartBDate,

    }
}
module.exports = nominationUtils






