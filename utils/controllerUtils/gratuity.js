const gratuityUtils = {}
gratuityUtils.createGratuityUtils = (emp, gratuityNomObj, granuityWitnessObj) => {
    let gratuityNominee = gratuityNomObj.map(gr => gr._id)
    let gratuityWitness = granuityWitnessObj.map(gr => gr._id)
    return {
        gnName: emp.gnName,
        gnNoticeDate: emp.gnNoticeDate,
        gnEmpName: emp.gnEmpName,
        gnEmpSex: emp.gnEmpSex,

        gnEmpReligion: emp.gnEmpReligion,
        gnEmpMartialStatus: emp.gnEmpMartialStatus,
        gnEmpDepartment: emp.gnEmpDepartment,
        gnEmpDesignation: emp.gnEmpDesignation,

        gnEmpAppointmentDate: emp.gnEmpAppointmentDate,
        gnEmpPerAdd: emp.gnPermAdd,
        gnEmpPreAdd: emp.gnPreAdd,
        gnPlace: emp.gnPlace,

        gnDate: emp.gnDate,
        gnSign: emp.gnSign,
        gratuityNominee,
        gratuityWitness

    }
}

gratuityUtils.createGratuityNomInput = (emp) => {
    return [

        {
            gnNomineeName: emp.gnNomineeName1,
            gnNomineeRelation: emp.gnNomineeRelation1,
            gnNomineeAge: emp.gnNomineeAge1,
            gnNomineeProportion: emp.gnNomineeProportion1,

        },

        {
            gnNomineeName: emp.gnNomineeName2,
            gnNomineeRelation: emp.gnNomineeRelation2,
            gnNomineeAge: emp.gnNomineeAge2,
            gnNomineeProportion: emp.gnNomineeProportion2,

        },

        {
            gnNomineeName: emp.gnNomineeName3,
            gnNomineeRelation: emp.gnNomineeRelation3,
            gnNomineeAge: emp.gnNomineeAge3,
            gnNomineeProportion: emp.gnNomineeProportion3,

        },

        {
            gnNomineeName: emp.gnNomineeName4,
            gnNomineeRelation: emp.gnNomineeRelation4,
            gnNomineeAge: emp.gnNomineeAge4,
            gnNomineeProportion: emp.gnNomineeProportion4,

        }
    ]
}

gratuityUtils.createGratuityWitnessInput = (emp) => {
    return [
        {
            gnWitnessName: emp.gnWitnessName1,
            gnWitnessSign: emp.gnWitnessSign1,


        },
        {
            gnWitnessName: emp.gnWitnessName2,
            gnWitnessSign: emp.gnWitnessSign2,

        }
    ]
}

module.exports = gratuityUtils


//  gnName: '',
//  gnNoticeDate: '',
//  gnNomineeName1: '',
//  gnNomineeRelation1: '',
//  gnNomineeAge1: '',
//  gnNomineeProportion1: '',
//  gnNomineeName2: '',
//  gnNomineeRelation2: '',
//  gnNomineeAge2: '',
//  gnNomineeProportion2: '',
//  gnNomineeName3: '',
//  gnNomineeRelation3: '',
//  gnNomineeAge3: '',
//  gnNomineeProportion3: '',
//  gnNomineeName4: '',
//  gnNomineeRelation4: '',
//  gnNomineeAge4: '',
//  gnNomineeProportion4: '',
//  gnEmpName: '',
//  gnEmpSex: '',
//  gnEmpReligion: '',
//  gnEmpMartialStatus: '',
//  gnEmpDepartment: '',
//  gnEmpDesignation: '',
//  gnEmpAppointmentDate: '',
//  gnPermAdd: '',
//  gnPreAdd: '',
//  gnPlace: '',
//  gnDate: '',
//  gnSign: '',
//  gnWitnessName1: '',
//  gnWitnessSign1: '',
//  gnWitnessName2: '',
//  gnWitnessSign2: '',