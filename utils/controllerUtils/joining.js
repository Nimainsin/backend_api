const joiningUtils = {}

joiningUtils.createJoinInput = (emp, joinEduObj, joinExpeObj, joinPersonalObj, joinMiscObj, filesObj) => {
    let joiningEducation = joinEduObj.map(edu => edu._id)
    let joinPersonal = joinPersonalObj.map(per => per._id)
    let joinExperience = joinExpeObj.map(exp => exp._id)
    let joinMisc = [joinMiscObj[0]._id]

    return {
        joinProfileImg: filesObj.image_file,
        joinPost: emp.joinPost,
        joinEmpName: emp.joinEmpName,
        joinFatherName: emp.joinFatherName,
        joinGender: emp.joinGender,
        joinMartial: emp.joinMartial,
        joinBirthday: emp.joinBirthday,
        joinPan: emp.joinPan,
        joinAadhar: emp.joinAadhar,
        joinBloodGroup: emp.joinBloodGroup,
        joinPhysicalDisable: emp.joinPhysicalDisable,
        joinPreAdd: emp.joinPreAdd,
        joinPermAdd: emp.joinPermAdd,
        joinMobNo: emp.joinMobNo,
        joinEmail: emp.joinEmail,
        joinKnownLanguage: emp.joinKnownLanguage,
        joiningEducation,
        joinPersonal,
        joinExperience,
        joinMisc

    }
}


joiningUtils.createJoinEduInput = (emp,) => {
    return [
        {
            examPassed: emp.examPassed1,
            examYear: emp.examYear1,
            college: emp.college1,
            university: emp.university1,
            marks: emp.marks1,
            subjects: emp.subjects1,

        },
        {
            examPassed: emp.examPassed2,
            examYear: emp.examYear2,
            college: emp.college2,
            university: emp.university2,
            marks: emp.marks2,
            subjects: emp.subjects2,
        },
        {
            examPassed: emp.examPassed3,
            examYear: emp.examYear3,
            college: emp.college3,
            university: emp.university3,
            marks: emp.marks3,
            subjects: emp.subjects3,
        },
        {
            examPassed: emp.examPassed4,
            examYear: emp.examYear4,
            college: emp.college4,
            university: emp.university4,
            marks: emp.marks4,
            subjects: emp.subjects4,
        }
    ]
}

joiningUtils.createJoinExperience = (emp) => {
    return [
        {
            org: emp.organization1,
            from: emp.from1,
            to: emp.to1,
            designation: emp.designation1,
            salary: emp.salary1,
            leaveReason: emp.leaveReason1,
        },
        {
            org: emp.organization2,
            from: emp.from2,
            to: emp.to2,
            designation: emp.designation2,
            salary: emp.salary2,
            leaveReason: emp.leaveReason2,

        },
        {
            org: emp.organization3,
            from: emp.from3,
            to: emp.to3,
            designation: emp.designation3,
            salary: emp.salary3,
            leaveReason: emp.leaveReason3,

        },
        {
            org: emp.organization4,
            from: emp.from4,
            to: emp.to4,
            designation: emp.designation4,
            salary: emp.salary4,
            leaveReason: emp.leaveReason4,

        }
    ]
}


joiningUtils.createPersonalInput = (emp) => {
    return [
        {
            familyMember: emp.familyMember1,
            gender: emp.gender1,
            dob: emp.dob1,
            relation: emp.reletionship1,
            occupation: emp.occupation1,
            dependent: emp.dependent1a

        },
        {
            familyMember: emp.familyMember2,
            gender: emp.gender2,
            dob: emp.dob2,
            relation: emp.reletionship2,
            occupation: emp.occupation2,
            dependent: emp.dependent2,

        },
        {
            familyMember: emp.familyMember3,
            gender: emp.gender3,
            dob: emp.dob3,
            relation: emp.reletionship3,
            occupation: emp.occupation3,
            dependent: emp.dependent3,

        },
        {
            familyMember: emp.familyMember4,
            gender: emp.gender4,
            dob: emp.dob1,
            relation: emp.reletionship4,
            occupation: emp.occupation4,
            dependent: emp.dependent4,

        }
    ]
}

joiningUtils.createJoinMiscInput = (emp) => {
    return {
        howKnew: emp.howKnew,
        relatedTo: emp.relatedTo,
        relatedInfo: emp.relatedInfo,
        hobbies: emp.hobbies,
        contactPerson: emp.contactPerson,
        contactNumber: emp.contactNumber,
        professionalRef1: emp.professionalRef1,
        professionalRef2: emp.professionalRef2,
        declarationDate: emp.declarationDate,
        declarationSign: emp.declarationSign,

    }
}

module.exports = joiningUtils






