exports.createFamilyInput = (emp, familyMemObj) => {
    let familyMember = familyMemObj.map(fm => fm._id)
    return {
        fdEmpName: emp.fdEmpName,
        fdEmpCode: emp.fdEmpCode,
        fdEmpJoinDate: emp.fdEmpJoinDate,
        fdEmpDesign: emp.fdEmpDesign,
        fdEmpLevel: emp.fdEmpLevel,
        fdEmpDepart: emp.fdEmpDepart,
        fdEmpLocation: emp.fdEmpLocation,
        fdEmpDob: emp.fdEmpDob,
        fdEmpAdd: emp.fdEmpAdd,
        fdEmpPin: emp.fdEmpPin,
        fdEmpTele: emp.fdEmpTele,
        fmDate: emp.fmDate,
        fmPlace: emp.fmPlace,
        fmSign: emp.fmSign,
        familyMember

    }
}

exports.createFamilyMemberInput = (emp) => {
    return [
        {
            familyMemberName: emp.familyMemberName1,
            familyMemberRelation: emp.familyMemberRelation1,
            familyMemberOccupation: emp.familyMemberOccupation1,
            familyMemberDob: emp.familyMemberDob1,

        },
        {
            familyMemberName: emp.familyMemberName2,
            familyMemberRelation: emp.familyMemberRelation2,
            familyMemberOccupation: emp.familyMemberOccupation2,
            familyMemberDob: emp.familyMemberDob2,

        },
        {
            familyMemberName: emp.familyMemberName3,
            familyMemberRelation: emp.familyMemberRelation3,
            familyMemberOccupation: emp.familyMemberOccupation3,
            familyMemberDob: emp.familyMemberDob3,

        },
        {
            familyMemberName: emp.familyMemberName4,
            familyMemberRelation: emp.familyMemberRelation4,
            familyMemberOccupation: emp.familyMemberOccupation4,
            familyMemberDob: emp.familyMemberDob4,

        }
    ]
}
//   fdEmpName: 'satish kumar',
//   fdEmpCode: '112',
//   fdEmpJoinDate: '12-12-12',
//   fdEmpDesign: 'SD',
//   fdEmpLevel: 'MID- SENIOR',
//   fdEmpDepart: 'IT',
//   fdEmpLocation: 'DELHI',
//   fdEmpDob: '12-12-12',
//   fdEmpAdd: 'PATNA',
//   fdEmpPin: '801108',
//   fdEmpTele: '123344',
//   familyMemberName1: 'SATISH KUMAR',
//   familyMemberRelation1: 'brother',
//   familyMemberOccupation1: 'aaa',
//   familyMemberDob1: '12-12-12 /12',
//   familyMemberName2: 'rakesh',
//   familyMemberRelation2: 'brithe',
//   familyMemberOccupation2: 'aass',
//   familyMemberDob2: 'df',
//   familyMemberName3: 'dfg',
//   familyMemberRelation3: 'err',
//   familyMemberOccupation3: 'sd',
//   familyMemberDob3: 'fg',
//   familyMemberName4: 'xcv',
//   familyMemberRelation4: 'fg',
//   familyMemberOccupation4: 'ghj',
//   familyMemberDob4: 'df',
//   fmDate: '12-12-12',
//   fmPlace: 'PATNA',
//   fmSign: 'SATISH KUMAR'