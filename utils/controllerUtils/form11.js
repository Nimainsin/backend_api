
const form11Utils = {}
form11Utils.createForm11Input = (emp) => {
    return {
        form11Name: emp.form11Name,
        form11Relation: emp.form11Relation,
        form11LastEmp: emp.form11LastEmp,
        form11LatEmpNameAdd: emp.form11LastEmpNameAdd,

        form11ResignDate: emp.form11ResignDate,
        form11LastEmpFrom: emp.form11LastEmpFrom,
        form11LastEmpTo: emp.form11LastEmpTo,
        form11PfMember: emp.form11Pfmember,

        form11PfName: emp.form11PfName,
        form11PfFrom: emp.form11PfFrom,
        form11PfTo: emp.form11PfTo,
        form11PfAcc: emp.form11pfAcc,

        form11PfWidhraw: emp.form11PfWidhraw,
        form11SuperAnnuationDrawn: emp.form11SuperannuationDrawn,
        form11NotPfMember: emp.form11NotPfMember,
        form11Sign: emp.form11Sign,
        form11Date: emp.form11Date,

    }
}

module.exports = form11Utils

//   form11Name: 'satish',
//   form11Relation: 'Sunita Devi',
//   form11LastEmp: 'on',
//   form11LastEmpNameAdd: 'adc & asdf',
//   form11ResignDate: '12-12-2020',
//   form11LastEmpFrom: '01-12-20',
//   form11LastEmpTo: '12-23-23',
//   form11Pfmember: 'on',
//   form11PfName: 'pf',
//   form11PfFrom: '12-12-12',
//   form11PfTo: '12-11-12',
//   form11pfAcc: '12345',
//   form11PfWidhraw: 'on',
//   form11SuperannuationDrawn: 'on',
//   form11NotPfMember: 'on',
//   form11Sign: 'satish kumar',
//   form11Date: '19-08-10',