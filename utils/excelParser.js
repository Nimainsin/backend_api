const excel = {};
const Exceljs = require("exceljs");
const csvJson = require("csvtojson");

/**
 * Parse excel file.
 *
 * @param {string} filePath
 *
 */

excel.parse = (filePath) =>
  new Promise((resolve, reject) => {
    csvJson()
      .fromFile(filePath)
      .then(
        (json) => {
          return resolve(json);
        },
        (err) => reject(err)
      );
  });

excel.createInviteTemplate = async (rows) => {
  let workbook = new Exceljs.Workbook();
  let sheet = workbook.addWorksheet("My Sheet");
  sheet.columns = [
    { header: "email", key: "email", width: 40 },
    { header: "role", key: "role", width: 18 },
  ];
  sheet.addRows(rows);
  // sheet.commit()
  return await workbook.csv.writeBuffer();
};

module.exports = excel;
