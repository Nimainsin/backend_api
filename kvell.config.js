module.exports = {
  protocol: "http",
  routes: [
    {
      name: "query",
      path: "/query",
    },
    {
      name: "vacancy",
      path: "/vacancy",
    },
    {
      name: "user",
      path: "/user",
    },
    {
      name: "login",
      path: "/login",
    },
    {
      name: "signUp",
      path: "/signUp",
    },
    {
      name: "video",
      path: "/video",
    },
    {
      name: "questionCategory",
      path: "/questionCategory",
    },
    {
      name: "answer",
      path: "/answer",
    },
    {
      name: "village",
      path: "/village",
    },
    {
      name: "state",
      path: "/state",
    },
    {
      name: "group",
      path: "/group",
    },
    {
      name: "district",
      path: "/district",
    },
    {
      name: "block",
      path: "/block",
    },
    {
      name: "gameContest",
      path: "/gameContest",
    },
    {
      name: "gameLevel",
      path: "/gameLevel",
    },
    {
      name: "obstacle",
      path: "/obstacle",
    },
    {
      name: "questionBank",
      path: "/questionBank",
    },
    {
      name: "memoryGame",
      path: "/memoryGame",
    },
    {
      name: "reward",
      path: "/reward",
    },
    {
      name: "role",
      path: "/role",
    },
    {
      name: "orgUser",
      path: "/orgUser",
    },
    {
      name: "orgUserRole",
      path: "/orgUserRole",
    },
    {
      name: "orgModule",
      path: "/orgModule",
    },

    {
      name: "modulePriviledge",
      path: "/modulePriviledge",
    },
    {
      name: "avatar",
      path: "/avatar",
    },

    {
      name: "otp",
      path: "/otp",
    },
    {
      name: "winner",
      path: "/winner",
    },
    {
      name: "limit",
      path: "/limit",
    },
    {
      name: "questionBankHindi",
      path: "/questionBankHindi",
    },

    {
      name: "memoryGameHindi",
      path: "/memoryGameHindi",
    },
    {
      name: "rank",
      path: "/rank",
    },
    {
      name: "count",
      path: "/count",
    },
    {
      name: "chat",
      path: "/chat",
    },
    {
      name: "chatgrp",
      path: "/chatgrp",
    },

    {
      name: "orgModule2",
      path: "/orgModule2",
    },

  ],

  models: [
    "vacancy",
    "query",
    "applyInVacancy",
    "user",
    "userRole",
    "rolePermission",
    "video",
    "questionCategory",
    "answer",
    "village",
    "state",
    "energy",
    "group",
    "district",
    "block",
    "gameContest",
    "gameLevel",
    "obstacle",
    "questionBank",
    "memoryGame",
    "reward",
    "role",
    "orgUser",
    "orgUserRole",
    "orgModule",
    "otp",
    "modulePriviledge",
    "login",
    "avatar",
    "winner",
    "limit",
    "questionBankHindi",
    "memoryGameHindi",
    "rank",
    "count",
    "chat",
    "chatgrp",
    "orgModule2"
  ],
  autoRequireRoutes: true,
  registerDocsRoute: true,
};
