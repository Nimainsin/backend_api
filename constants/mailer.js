const mailer = {}
const config = require("../config")
const nodemailer = require('nodemailer')

mailer.mailSubject = {

  CONTACT: {
    subject: "",
    template: "visitor-query",
  },
  RESET_PASSWORD: {
    subject: "[OXFAM] Reset Password",
    template: "",
  }
}

// Instantiate the SMTP server
mailer.smtpTrans = nodemailer.createTransport({
  host: config.EMAIL_HOST,
  port: config.EMAIL_PORT,
  secure: config.SECURE,
  auth: {
    user: config.EMAIL_USER,
    pass: config.EMAIL_PASS
  }
})

module.exports = mailer

