const gameLevelRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const gameLevelController = require("../controllers").gameLevel

const { celebrate, Joi, errors } = require('celebrate');

gameLevelRouter.get("/getAllGameLevels", (request, response) => {
  gameLevelController.getAllGameLevel().then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

gameLevelRouter.get("/getGameLevel", celebrate({
  query: {
    id: Joi.string().required()
  }
}), errors(), (request, response) => {
  gameLevelController.getGameLevel(request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

gameLevelRouter.post("/createGameLevel", celebrate({
  body: Joi.object().keys({
    name: Joi.string().required(),
    gameLevel: Joi.number().valid(1, 2, 3, 4),
    obstacleIds: Joi.array().required(),
    rewardIds: Joi.array().required(),
    memoryGameIds: Joi.array().required(),
    contestIds: Joi.array().required(),
  }),
}), errors(), (request, response) => {
  gameLevelController.insertGameLevel(request.body).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
})

gameLevelRouter.put("/editGameLevel", celebrate({
  query: {
    id: Joi.string().required()
  },
  body: Joi.object().keys({
    // fields: Joi.object().keys({
    name: Joi.string(),
    gameLevel: Joi.number().valid(1, 2, 3, 4),
    obstacleIds: Joi.array(),
    rewardIds: Joi.array(),
    memoryGameIds: Joi.array(),
    contestIds: Joi.array(),

    // }),

  }),
}), errors(), (request, response) => {
  gameLevelController.editGameLevel(request.body, request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
})

gameLevelRouter.delete("/deleteGameLevel", celebrate({
  query: {
    id: Joi.string().required()
  }

}), errors(), (request, response) => {
  gameLevelController.deleteGameLevel(request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

module.exports = gameLevelRouter;