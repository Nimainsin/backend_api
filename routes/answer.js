const answerRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const answerController = require("../controllers").answer;

const { celebrate, Joi, errors } = require('celebrate');

answerRouter.get("/getAllAnswers", (request, response) => {
  answerController.getAllAnswer().then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

answerRouter.get("/getAnswer", celebrate({
  query: {
    id: Joi.string().required()
  }
}), errors(), (request, response) => {
  answerController.getAnswer(request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

answerRouter.post("/createAnswer", celebrate({
  body: Joi.object().keys({
    fields: Joi.object().keys({
      details: Joi.string().required(),
      type: Joi.string().valid("a", "b").required(),
    }),
    files: Joi.object()

  }),
}), errors(), (request, response) => {
  answerController.insertAnswer(request.body.fields).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

answerRouter.put("/editAnswer", celebrate({
  query: {
    id: Joi.string().required()
  },
  body: Joi.object().keys({
    fields: Joi.object().keys({
      details: Joi.string().required(),
      type: Joi.string().valid("a", "b").required(),
    }),
    files: Joi.object()
  }),
}), errors(), (request, response) => {
  answerController.editAnswer(request.body.fields, request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
})

answerRouter.delete("/deleteAnswer", celebrate({
  query: {
    id: Joi.string().required()
  }

}), errors(), (request, response) => {
  answerController.deleteAnswer(request.query.id).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

module.exports = answerRouter;