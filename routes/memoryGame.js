const memoryGameRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const memoryGameController = require("../controllers").memoryGame;
const { Joi, celebrate, errors } = require("celebrate");

memoryGameRouter.get("/getAllMemoryGames", (request, response) => {
  memoryGameController.getAllMemoryGames(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

memoryGameRouter.get(
  "/getMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameController.getMemoryGame(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameRouter.post(
  "/insertMemoryGame",
  celebrate({
    body: Joi.object().keys({
      question: Joi.array().required(),
      name: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameController.insertMemoryGame(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameRouter.put(
  "/editMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
    body: Joi.object().keys({
      question: Joi.array(),
      name: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameController.editMemoryGame(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameRouter.delete(
  "/deleteMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameController.deleteMemoryGame(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = memoryGameRouter;
