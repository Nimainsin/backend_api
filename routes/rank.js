const rankRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const rankController = require("../controllers").rank;
const { celebrate, Joi, errors } = require("celebrate");

rankRouter.get("/getAllRanks",

  celebrate({
    query: {
      contestId: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    rankController.getAllRank(request.query.contestId).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);



rankRouter.get("/getAllRank",

  celebrate({
    query: {
      contestId: Joi.string().required(),
      page: Joi.number(),
      perPage: Joi.number(),
    },
  }),
  errors(),

  (request, response) => {
    rankController.getAllRanks(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  });





rankRouter.get(
  "/getAllPlayerPosition",
  celebrate({
    query: {
      contestId: Joi.string().required(),
      page: Joi.number(),
      perPage: Joi.number(),
    },
  }),
  errors(),
  (request, response) => {
    rankController.getAllPlayerRank(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


rankRouter.get(
  "/getRank",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    rankController.getRank(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


rankRouter.post(
  "/createRank",
  celebrate({
    body: Joi.object().keys({
      contestId: Joi.string().required(),
      userId: Joi.string().required(),
      position: Joi.string(),
      levelname: Joi.string(),
      energy: Joi.string(),
      points: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    rankController.insertRank(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


rankRouter.post(
  "/editRank",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      contestId: Joi.string(),
      userId: Joi.string(),
      position: Joi.string(),
      levelname: Joi.string(),
      energy: Joi.string(),
      points: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    rankController.editRank(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

rankRouter.delete(
  "/deleteRank",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    rankController.deleteRank(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);





rankRouter.post("/", (request, response) => {
  // post method
});

rankRouter.put("/", (request, response) => {
  // put method
});

rankRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = rankRouter;