const chatgrpRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const chatgrpController = require("../controllers").chatgrp;

const { celebrate, Joi, errors } = require("celebrate");




chatgrpRouter.get("/getAllChats",
  celebrate({
    query: Joi.object().keys({
      contestId: Joi.string().required(),
      groupId: Joi.string().required(),
    })
  }), errors()
  , (request, response) => {
    chatgrpController.getAllgrpChats(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  });


  chatgrpRouter.post(
  "/creategrpChat",
  celebrate({
    body: Joi.object().keys({
      contestId: Joi.string().required(),
      userId: Joi.string().required()
    }),
  }),
  errors(),
  (request, response) => {
    chatgrpController.insertgrpChat(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);



module.exports = chatgrpRouter;














