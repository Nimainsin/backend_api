const rewardRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const rewardController = require("../controllers").reward;
const { celebrate, Joi, errors } = require("celebrate");

rewardRouter.get("/getAllRewards", (request, response) => {
  rewardController.getAllReward(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

rewardRouter.get(
  "/getReward",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    rewardController.getReward(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

rewardRouter.post(
  "/createReward",
  celebrate({
    body: Joi.object().keys({
      fields: Joi.object().keys({
        name: Joi.string().required(),
        point: Joi.number().required(),
      }),
      files: Joi.object().keys({
        image: Joi.object(),
      }),
    }),
  }),
  errors(),
  (request, response) => {
    rewardController.insertReward(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

rewardRouter.put(
  "/editReward",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      fields: Joi.object().keys({
        name: Joi.string(),
        point: Joi.string(),
      }),
      files: Joi.object().keys({
        image: Joi.object(),
      }),
    }),
  }),
  errors(),
  (request, response) => {
    rewardController
      .editReward({ ...request.body, _id: request.query.id })
      .then(
        (res) => response.status(res.status).json(res),
        (err) => response.status(err.status).json(err)
      );
  }
);

rewardRouter.delete(
  "/deleteReward",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    rewardController.deleteReward(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = rewardRouter;
