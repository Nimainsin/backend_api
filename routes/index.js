const query = require("./query");
const vacancy = require("./vacancy");
const user = require("./user");
const login = require("./login");
const signUp = require("./signUp");
const video = require("./video");
const questionCategory = require("./questionCategory");
const answer = require("./answer");
const village = require("./village");
const state = require("./state");
const group = require("./group");
const district = require("./district");
const block = require("./block");
const gameContest = require("./gameContest");
const gameLevel = require("./gameLevel");
const obstacle = require("./obstacle");
const questionBank = require("./questionBank");
const memoryGame = require("./memoryGame");
const reward = require("./reward");
const role = require("./role");
const orgUser = require("./orgUser");
const orgUserRole = require("./orgUserRole");
const orgModule = require("./orgModule");
const modulePriviledge = require("./modulePriviledge");
const avatar = require("./avatar");
const otp = require("./otp");
const winner = require("./winner");
const limit = require("./limit");
const questionBankHindi = require("./questionBankHindi");
const memoryGameHindi = require("./memoryGameHindi");
const rank = require("./rank");
const count = require("./count");
const chat = require("./chat");
const chatgrp = require("./chatgrp");
const orgModule2 = require("./orgModule2");

module.exports = {
  query,
  vacancy,
  user,
  login,
  signUp,
  video,
  questionCategory,
  answer,
  village,
  state,
  group,
  district,
  block,
  gameContest,
  gameLevel,
  obstacle,
  questionBank,
  memoryGame,
  reward,
  role,
  orgUser,
  orgUserRole,
  orgModule,
  modulePriviledge,
  avatar,
  otp,
  winner,
  limit,
  questionBankHindi,
  memoryGameHindi,
  rank,
  count,
  chat,
  chatgrp,
  orgModule2
};