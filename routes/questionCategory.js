const questionCategoryRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const questionCategoryController = require("../controllers").questionCategory;
const { celebrate, Joi, errors } = require("celebrate");

questionCategoryRouter.get("/getAllQuestionCategory", (request, response) => {
  questionCategoryController.getAllQuestionCategory(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionCategoryRouter.get(
  "/getQuestionCategory",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    questionCategoryController.getQuestionCategory(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionCategoryRouter.post(
  "/createQuestionCategory",
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required().valid("Health", "Entertainment"),
    }),
  }),
  errors(),
  (request, response) => {
    questionCategoryController.insertQuestionCategory(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionCategoryRouter.put(
  "/editQuestionCategory",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      fields: Joi.object().keys({
        name: Joi.string().valid("Health", "Entertainment"),
      }),
      files: Joi.object(),
    }),
  }),
  errors(),
  (request, response) => {
    questionCategoryController
      .editQuestionCategory(request.body.fields, request.query.id)
      .then(
        (res) => response.status(res.status).json(res),
        (err) => response.status(err.status).json(err)
      );
  }
);

questionCategoryRouter.delete(
  "/deleteQuestionCategory",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    questionCategoryController.deleteQuestionCategory(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = questionCategoryRouter;
