const blockRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const blockController = require("../controllers").block;

const { celebrate, Joi, errors } = require("celebrate");

blockRouter.get("/getAllBlocks", (request, response) => {
  blockController.getAllBlock(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

blockRouter.get("/getAllBlocksByDistrict/:id", celebrate({
  params: Joi.object().keys({
    id: Joi.string().required()
  }),

}), errors(), (request, response) => {
  blockController.getAllBlocksByDistrict({ ...request.query, districtId: request.params.id }).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

blockRouter.get(
  "/getBlock",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    blockController.getBlock(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

blockRouter.post(
  "/createBlock",
  celebrate({
    body: Joi.object().keys({
      // fields: Joi.object().keys({
      name: Joi.string().required(),
      districtId: Joi.string().required(),
      //}),
      //files: Joi.object(),
    }),
  }),
  errors(),
  (request, response) => {
    blockController.insertBlock(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

blockRouter.put(
  "/editBlock",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({

      name: Joi.string(),
      districtId: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    blockController.editBlock(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

blockRouter.delete(
  "/deleteBlock",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    blockController.deleteBlock(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);
module.exports = blockRouter;
