const limitRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const limitController = require("../controllers").limit;
const { celebrate, Joi, errors } = require("celebrate");

limitRouter.get("/getAllLimits", (request, response) => {
  limitController.getAllLimit(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

limitRouter.get(
  "/getLimit",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    limitController.getLimit(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

limitRouter.post(
  "/createLimit",
  celebrate({
    body: Joi.object().keys({
      limit: Joi.number().required(),

    }),
  }),
  errors(),
  (request, response) => {
    limitController.insertLimit(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

limitRouter.put(
  "/editLimit",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      limit: Joi.number()
    }),
  }),
  errors(),
  (request, response) => {
    limitController.editLimit(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

limitRouter.delete(
  "/deleteLimit",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    limitController.deleteLimit(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = limitRouter;