const gameContestRouter = require('kvell-scripts').router()
// eslint-disable-next-line no-unused-vars
const gameContestController = require('../controllers').gameContest

const { celebrate, Joi, errors } = require('celebrate')

gameContestRouter.get('/getAllContests', (request, response) => {
  gameContestController.getAllContest(request.query).then(
    res => response.status(res.status).json(res),
    err => response.status(err.status).json(err)
  )
})

gameContestRouter.get('/getAllContestsInHindi', (request, response) => {
  gameContestController.getAllContestInHindi(request.query).then(
    res => response.status(res.status).json(res),
    err => {
      console.log('error', err)
      response.status(err.status).json(err)
    }
  )
})

gameContestRouter.post('/getTranslatedData', (request, response) => {
  gameContestController.translateText(request.query).then(
    res => response.status(res.status).json(res),
    err => {
      console.log('error', err)
      response.status(err.status).json(err)
    }
  )
})

gameContestRouter.post('/getMultiLangtranslateText', (request, response) => {
  console.log('request', request.body)
  gameContestController.MultiLangtranslateText(request.body).then(
    res => response.status(res.status).json(res),
    err => {
      console.log('error', err)
      response.status(err.status).json(err)
    }
  )
})

gameContestRouter.get(
  '/getContest',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    gameContestController.getContest(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

gameContestRouter.post(
  '/createContest',
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string(),
      image: Joi.string().allow(''),
      groupIds: Joi.array().items(Joi.string()).required(),
      maximumGroup: Joi.number().valid(2, 4, 8, 16, 32),
      winningAmount: Joi.number(),
      contestStartDate: Joi.date(),
      contestStartTime: Joi.string(),
      contestEndDate: Joi.date(),
      contestEndTime: Joi.string(),
      status: Joi.string().valid('active', 'inactive'),
    }),
  }),
  errors(),
  (request, response) => {
    gameContestController.insertContest(request.body).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

gameContestRouter.put(
  '/editContest',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      name: Joi.string(),
      description: Joi.string(),
      image: Joi.string().allow(''),
      groupIds: Joi.array().items(Joi.string()),
      maximumGroup: Joi.number().valid(2, 4, 8, 16, 32),
      winningAmount: Joi.number(),
      contestStartDate: Joi.date(),
      contestStartTime: Joi.string(),
      contestEndDate: Joi.date(),
      contestEndTime: Joi.string(),
      status: Joi.string().valid('active', 'inactive'),
    }),
  }),
  errors(),
  (request, response) => {
    gameContestController.editContest(request.body, request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

gameContestRouter.delete(
  '/deleteContest',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    gameContestController.deleteContest(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

gameContestRouter.get(
  '/getContestsByUserId',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    gameContestController.getContestsByUserId(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

module.exports = gameContestRouter
