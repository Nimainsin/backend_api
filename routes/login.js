const loginRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const loginController = require("../controllers").login;
const { celebrate, Joi, errors } = require("celebrate");

loginRouter.get("/", (request, response) => {
  // get method
});

loginRouter.post(
  "/",
  celebrate({
    body: Joi.object().keys({
      mobile: Joi.number().integer().required(),
      //otp: Joi.number().integer(),
    }),
  }),
  errors(),
  (request, response) => {
    loginController.login(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

loginRouter.post(
  "/orgUserLogin",
  celebrate({
    body: Joi.object().keys({
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    loginController.orgUserLogin(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

loginRouter.post(
  "/orgUserForgetPass",
  celebrate({
    body: Joi.object().keys({
      email: Joi.string().email().required(),
    }),
  }),
  errors(),
  (request, response) => {
    loginController.orgUserForgetPass(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

loginRouter.put("/", (request, response) => {
  // put method
});

loginRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = loginRouter;
