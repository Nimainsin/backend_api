const orgUserRouter = require('kvell-scripts').router()
// eslint-disable-next-line no-unused-vars
const orgUserController = require('../controllers').orgUser

const { celebrate, Joi, errors } = require('celebrate')

orgUserRouter.get('/getAllOrgUsers', (request, response) => {
  orgUserController.getAllOrgUser().then(
    res => {
      return response.status(res.status).json(res)
    },
    err => response.status(err.status).json(err)
  )
})

orgUserRouter.get(
  '/getOrgUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgUserController.getOrgUser(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRouter.post(
  '/createOrgUser',
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
      roleId: Joi.string().required(),
      priviledge: Joi.array().items(
        Joi.object().keys({
          moduleId: Joi.string().required(),
          read: Joi.boolean().default(false),
          write: Joi.boolean().default(false),
          create: Joi.boolean().default(false),
          delete: Joi.boolean().default(false),
        })
      ),
    }),
  }),
  errors(),
  (request, response) => {
    orgUserController.insertOrgUser(request.body).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRouter.post(
  '/getOrgUserByToken',
  celebrate({
    body: Joi.object().keys({
      token: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    orgUserController.getOrgUserByToken(request.body).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRouter.put(
  '/editOrgUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      name: Joi.string(),
      email: Joi.string().email(),
      password: Joi.string(),
      roleId: Joi.string(),
      priviledge: Joi.array().items(
        Joi.object().keys({
          moduleId: Joi.string(),
          read: Joi.boolean(),
          write: Joi.boolean(),
          create: Joi.boolean(),
          delete: Joi.boolean(),
        })
      ),
    }),
  }),
  errors(),
  (request, response) => {
    orgUserController.editOrgUser(request.body, request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRouter.delete(
  '/deleteOrgUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgUserController.deleteOrgUser(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRouter.get(
  '/undoOrgUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgUserController.undoOrgUser(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

module.exports = orgUserRouter
