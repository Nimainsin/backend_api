const winnerRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const winnerController = require("../controllers").winner;
const { celebrate, Joi, errors } = require("celebrate");

winnerRouter.get(
  "/getAllWinners",
  celebrate({
    query: {
      contestId: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    winnerController.getAllWinner(request.query.contestId).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


winnerRouter.get(
  "/getAllWinnerGroupRank",
  celebrate({
    query: {
      contestId: Joi.string().required(),
      page: Joi.number(),
      perPage: Joi.number(),
    },
  }),
  errors(),
  (request, response) => {
    winnerController.getAllWinnerGroupRank(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

winnerRouter.get(
  "/getAllPlayerRank",
  celebrate({
    query: {
      contestId: Joi.string().required(),
      page: Joi.number(),
      perPage: Joi.number(),
    },
  }),
  errors(),
  (request, response) => {
    winnerController.getAllPlayerRank(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

winnerRouter.get(
  "/getWinner",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    winnerController.getWinner(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

winnerRouter.post(
  "/createWinner",
  celebrate({
    body: Joi.object().keys({
      contestId: Joi.string().required(),
      userId: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    winnerController.insertWinner(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

winnerRouter.put(
  "/editWinner",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      contestId: Joi.string(),
      userId: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    winnerController.editWinner(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

winnerRouter.delete(
  "/deleteWinner",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    winnerController.deleteWinner(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = winnerRouter;
