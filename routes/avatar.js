const avatarRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const avatarController = require("../controllers").avatar;
const { celebrate, Joi, errors } = require("celebrate");

avatarRouter.get("/getAllAvatars", (request, response) => {
  avatarController.getAllAvatar(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

avatarRouter.get(
  "/getAvatar",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    avatarController.getAvatar(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

avatarRouter.post(
  "/createAvatar",
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    avatarController.insertAvatar(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

avatarRouter.put("/editAvatar", celebrate({
  query: {
    id: Joi.string().required(),
  },
  body: Joi.object().keys({
    name: Joi.string(),
  }),
}),
  errors(),
  (request, response) => {
    avatarController.editAvatar(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

avatarRouter.delete("/deleteAvatar", celebrate({
  query: {
    id: Joi.string().required(),
  },
}),
  errors(),
  (request, response) => {
    avatarController.deleteAvatar(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = avatarRouter;