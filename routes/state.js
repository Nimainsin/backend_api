const stateRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const stateController = require("../controllers").state;
const { celebrate, Joi, errors } = require("celebrate");

stateRouter.get(
  "/getAllStates",
  // celebrate({
  //   query: {
  //     skip: Joi.string(),
  //     limit: Joi.string(),
  //   }
  // }), errors(),
  (request, response) => {
    stateController.getAllstate(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

stateRouter.get(
  "/getState",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    stateController.getstate(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

stateRouter.post(
  "/createState",
  celebrate({
    body: Joi.object().keys({
      // fields: Joi.object().keys({
      name: Joi.string().required(),
      // }),
      // files: Joi.object()
    }),
  }),
  errors(),
  (request, response) => {
    stateController.insertState(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

stateRouter.put(
  "/editState",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      // fields: Joi.object().keys({
      name: Joi.string(),
      // }),
      // files: Joi.object()
    }),
  }),
  errors(),
  (request, response) => {
    stateController.editstate(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

stateRouter.delete(
  "/deleteState",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    stateController.deletestate(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = stateRouter;
