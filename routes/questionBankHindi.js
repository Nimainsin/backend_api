const questionBankHindiRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const questionBankHindiController = require("../controllers").questionBankHindi;

const { celebrate, Joi, errors } = require("celebrate");

questionBankHindiRouter.get("/getAllQuestions", (request, response) => {
  questionBankHindiController.getAllQuestions(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankHindiRouter.get("/getAllQuestionsInHindi", (request, response) => {
  questionBankHindiController.getAllQuestionsInHindi(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankHindiRouter.get("/getQuestionsWithAnswer", (request, response) => {
  questionBankHindiController.getQuestionsWithAnswer().then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankHindiRouter.get(
  "/getQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.getQuestion(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankHindiRouter.post(
  "/insertQuestion",
  celebrate({
    body: Joi.object().keys({
      questionTitle: Joi.string().required(),
      option1: Joi.string().required(),
      option2: Joi.string().required(),
      option3: Joi.string().default(null),
      option4: Joi.string().default(null),
      questionCategoryId: Joi.string().required(),
      questionCoinPoints: Joi.number().required(),
      multipleChoice: Joi.boolean(),
      answer: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.insertQuestion(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankHindiRouter.post(
  "/insertBulkQuestion",
  celebrate({
    body: Joi.object().keys({
      fields: Joi.object().keys({

      }).unknown(),
      files: Joi.object().keys({
        file: Joi.object().required()
      })
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.insertBulkQuestion(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankHindiRouter.put(
  "/editQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
    body: Joi.object().keys({
      questionTitle: Joi.string(),
      option1: Joi.string(),
      option2: Joi.string(),
      option3: Joi.string(),
      option4: Joi.string(),
      questionCategoryId: Joi.string(),
      questionCoinPoints: Joi.number(),
      multipleChoice: Joi.boolean(),
      answer: Joi.string().valid("option1", "option2", "option3", "option4"),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.editQuestion(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankHindiRouter.delete(
  "/deleteQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.removeQuestion(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankHindiRouter.post(
  "/verifyAnswer",
  celebrate({
    body: Joi.object().keys({
      questionId: Joi.string().required(),
      answer: Joi.array().items("option1", "option2", "option3", "option4"),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankHindiController.verifyAnswer(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = questionBankHindiRouter;