const villageRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const villageController = require("../controllers").village;
const { celebrate, Joi, errors } = require("celebrate");

villageRouter.get("/getAllVillages", (request, response) => {
  villageController.getAllVillage(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

villageRouter.get("/getAllVillagesByBlock/:id", celebrate({
  params: Joi.object().keys({
    id: Joi.string().required()
  })
}), (request, response) => {
  villageController.getAllVillagesByBlock({ blockId: request.params.id, ...request.query }).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

villageRouter.get(
  "/getVillage",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    villageController.getVillage(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

villageRouter.post(
  "/createVillage",
  celebrate({
    body: Joi.object().keys({
      //fields: Joi.object().keys({
      name: Joi.string().required(),
      blockId: Joi.string().required(),
      // }),
      // files: Joi.object()
    }),
  }),
  errors(),
  (request, response) => {
    villageController.insertVillage(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

villageRouter.put(
  "/editVillage",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      // fields: Joi.object().keys({
      name: Joi.string(),
      blockId: Joi.string(),
      // }),
      // files: Joi.object()
    }),
  }),
  errors(),
  (request, response) => {
    villageController.editVillage(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

villageRouter.delete(
  "/deleteVillage",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    villageController.deleteVillage(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = villageRouter;
