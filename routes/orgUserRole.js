const orgUserRoleRouter = require('kvell-scripts').router()
// eslint-disable-next-line no-unused-vars
const orgUserRoleController = require('../controllers').orgUserRole
const { celebrate, Joi, errors } = require('celebrate')

orgUserRoleRouter.get('/getAllOrgUserRoles', (request, response) => {
  orgUserRoleController.getAllOrgUserRole(request.query).then(
    res => response.status(res.status).json(res),
    err => response.status(err.status).json(err)
  )
})

orgUserRoleRouter.get(
  '/getOrgRoleUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgUserRoleController.getOrgUserRole(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRoleRouter.post(
  '/createOrgRoleUser',
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      module: Joi.array().required(),
    }),
  }),
  errors(),
  (request, response) => {
    orgUserRoleController.insertOrgUserRole(request.body).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRoleRouter.put(
  '/editOrgRoleUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      name: Joi.string(),
      module: Joi.array().required(),
      orgModuleRoleData: Joi.array().required(),
    }),
  }),
  errors(),
  (request, response) => {
    orgUserRoleController.editOrgUserRole(request.body, request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

orgUserRoleRouter.delete(
  '/deleteOrgRoleUser',
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgUserRoleController.deleteOrgUserRole(request.query.id).then(
      res => response.status(res.status).json(res),
      err => response.status(err.status).json(err)
    )
  }
)

module.exports = orgUserRoleRouter
