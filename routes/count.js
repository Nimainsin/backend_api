const countRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const countController = require("../controllers").count;

countRouter.get("/", (request, response) => {
  countController.getCount().then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

countRouter.post("/", (request, response) => {
  // post method
});

countRouter.put("/", (request, response) => {
  // put method
});

countRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = countRouter;