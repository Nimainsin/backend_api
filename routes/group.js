const groupRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const groupController = require("../controllers").group;

const { celebrate, Joi, errors } = require("celebrate");

groupRouter.get("/getAllGroups", (request, response) => {
  groupController.getAllGroup(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

groupRouter.get(
  "/getGroup",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    groupController.getGroup(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

groupRouter.post(
  "/createGroup",
  celebrate({
    body: Joi.object().keys({
      name: Joi.string().required(),
      image: Joi.string().allow(""),
      villageId: Joi.string().required(),
      userIds: Joi.array().items(Joi.string()),
      maxOccupancy: Joi.number().required(),
      groupsPoints: Joi.number().required(),
      winnerPosition: Joi.string().valid("1st", "2nd", "3rd", "4th"),
      color: Joi.string()
    }),
  }),
  errors(),
  (request, response) => {
    groupController.insertGroup(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

groupRouter.put(
  "/editGroup",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      name: Joi.string(),
      image: Joi.string().allow(""),
      villageId: Joi.string(),
      userIds: Joi.array().items(Joi.string()),
      maxOccupancy: Joi.number(),
      groupsPoints: Joi.number(),
      winnerPosition: Joi.string().valid("1st", "2nd", "3rd", "4th"),
      color: Joi.string()
    }),
  }),
  errors(),
  (request, response) => {
    groupController.editGroup(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

groupRouter.delete(
  "/deleteGroup",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    groupController.deleteGroup(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = groupRouter;
