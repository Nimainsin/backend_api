const modulePriviledgeRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const modulePriviledgeController = require("../controllers").modulePriviledge;
const { celebrate, Joi, errors } = require("celebrate");

modulePriviledgeRouter.get("/getAllPriviledges", (request, response) => {
  modulePriviledgeController.getAllModulePriviledge(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

modulePriviledgeRouter.get(
  "/getPriviledge",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    modulePriviledgeController.getModulePriviledge(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

modulePriviledgeRouter.post(
  "/createPriviledge",
  celebrate({
    body: Joi.object().keys({
      moduleId: Joi.string().required(),
      read: Joi.boolean(),
      write: Joi.boolean(),
      create: Joi.boolean(),
      delete: Joi.boolean(),
      permissionAssignedBy: Joi.string().required(),
      permissionAssignTo: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    modulePriviledgeController.insertModulePriviledge(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

modulePriviledgeRouter.put(
  "/editPriviledge",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      moduleId: Joi.string(),
      read: Joi.boolean(),
      write: Joi.boolean(),
      create: Joi.boolean(),
      delete: Joi.boolean(),
      permissionAssignedBy: Joi.string(),
      permissionAssignTo: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    modulePriviledgeController
      .editModulePriviledge(request.body, request.query.id)
      .then(
        (res) => response.status(res.status).json(res),
        (err) => response.status(err.status).json(err)
      );
  }
);

modulePriviledgeRouter.delete(
  "/deletePriviledge",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    modulePriviledgeController.deleteModulePriviledge(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = modulePriviledgeRouter;
