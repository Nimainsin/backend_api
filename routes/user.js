const userRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const userController = require("../controllers").user;
const { celebrate, Joi, errors } = require("celebrate");

userRouter.get("/getAllUsers", (request, response) => {
  userController.getAllUser().then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

userRouter.get(
  "/getUser",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    userController.getUser(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

userRouter.get(
  "/inactiveUser",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    userController.deactivateUser(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

userRouter.get(
  "/getUserByVillage",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    userController.getUserByVillage(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

userRouter.post(
  "/editUser",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      mobile: Joi.number(),
      email: Joi.string(),
      name: Joi.string(),
      age: Joi.string(),
      gender: Joi.string().valid("male", "female", "trans gender"),
      userPoints: Joi.number().optional().allow(''),
      role: Joi.string().valid("user", "admin", "super admin"),
      otpToken: Joi.string(),
      countryCode: Joi.string().optional().allow(''),
      status: Joi.string().valid("active", "inactive").default('active'),
      energy: Joi.string(),
      imageUrl: Joi.string().optional().allow(''),
      villageId: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    userController.editUser(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

userRouter.post(
  "/setAvatar",
  celebrate({
    body: Joi.object().keys({
      userId: Joi.string().required(),
      avatarId: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    userController.setAvatar(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


userRouter.delete(
  "/deleteUser",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    userController.deleteUser(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = userRouter;
