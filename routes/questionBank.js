const questionBankRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const questionBankController = require("../controllers").questionBank;
const { celebrate, Joi, errors } = require("celebrate");

questionBankRouter.get("/getAllQuestions", (request, response) => {
  questionBankController.getAllQuestions(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankRouter.get("/getAllQuestionsInHindi", (request, response) => {
  questionBankController.getAllQuestionsInHindi(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankRouter.get("/getQuestionsWithAnswer", (request, response) => {
  questionBankController.getQuestionsWithAnswer().then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

questionBankRouter.get(
  "/getQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.getQuestion(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankRouter.post(
  "/insertQuestion",
  celebrate({
    body: Joi.object().keys({
      questionTitle: Joi.string().required(),
      option1: Joi.string().required(),
      option2: Joi.string().required(),
      option3: Joi.string().default(null),
      option4: Joi.string().default(null),
      questionCategoryId: Joi.string().required(),
      questionCoinPoints: Joi.number().required(),
      multipleChoice: Joi.boolean(),
      answer: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.insertQuestion(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankRouter.post(
  "/insertBulkQuestion",
  celebrate({
    body: Joi.object().keys({
      fields: Joi.object().keys({

      }).unknown(),
      files: Joi.object().keys({
        file: Joi.object().required()
      })
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.insertBulkQuestion(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankRouter.put(
  "/editQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
    body: Joi.object().keys({
      questionTitle: Joi.string(),
      option1: Joi.string(),
      option2: Joi.string(),
      option3: Joi.string(),
      option4: Joi.string(),
      questionCategoryId: Joi.string(),
      questionCoinPoints: Joi.number(),
      multipleChoice: Joi.boolean(),
      answer: Joi.string().valid("option1", "option2", "option3", "option4"),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.editQuestion(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankRouter.delete(
  "/deleteQuestion",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.removeQuestion(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

questionBankRouter.post(
  "/verifyAnswer",
  celebrate({
    body: Joi.object().keys({
      questionId: Joi.string().required(),
      answer: Joi.array().items("option1", "option2", "option3", "option4"),
    }),
  }),
  errors(),
  (request, response) => {
    questionBankController.verifyAnswer(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = questionBankRouter;
