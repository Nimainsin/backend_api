const memoryGameHindiRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const memoryGameHindiController = require("../controllers").memoryGameHindi;

const { Joi, celebrate, errors } = require("celebrate");

memoryGameHindiRouter.get("/getAllMemoryGames", (request, response) => {
  memoryGameHindiController.getAllMemoryGames(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

memoryGameHindiRouter.get(
  "/getMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameHindiController.getMemoryGame(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameHindiRouter.post(
  "/insertMemoryGame",
  celebrate({
    body: Joi.object().keys({
      question: Joi.array().required(),
      name: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameHindiController.insertMemoryGame(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameHindiRouter.put(
  "/editMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
    body: Joi.object().keys({
      question: Joi.array(),
      name: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameHindiController.editMemoryGame(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

memoryGameHindiRouter.delete(
  "/deleteMemoryGame",
  celebrate({
    query: Joi.object().keys({
      id: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    memoryGameHindiController.deleteMemoryGame(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


module.exports = memoryGameHindiRouter;