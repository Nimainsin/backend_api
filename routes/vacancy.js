const vacancyRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const vacancyController = require("../controllers").vacancy;

vacancyRouter.get("/getVacancy", (request, response) => {
  vacancyController.getVacancy(request.query._id).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

vacancyRouter.get("/getAllVacancy", (request, response) => {
  vacancyController.getAllVacancy().then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

vacancyRouter.post("/insertVacancy", (request, response) => {
  vacancyController.insertVacancy(request.body).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

vacancyRouter.post("/apply", (request, response) => {
  vacancyController.applyForVacancy(request.body).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

vacancyRouter.put("/editVacancy", (request, response) => {
  vacancyController.editVacancy(request.body, request.query._id).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

vacancyRouter.delete("/deleteVacancy", (request, response) => {
  vacancyController.deleteVacancy(request.query._id).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

module.exports = vacancyRouter;
