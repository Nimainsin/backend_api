const signUpRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const signUpController = require("../controllers").signUp;
const { celebrate, Joi, errors } = require("celebrate");

signUpRouter.post(
  "/verify",
  celebrate({
    body: Joi.object().keys({
      mobile: Joi.number().required(),
      otpToken: Joi.number().required(),
    }),
  }),
  (request, response) => {
    signUpController.verify(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

signUpRouter.post(
  "/",
  celebrate({
    body: Joi.object().keys({
      // fields: Joi.object().keys({
      mobile: Joi.number().required(),
    }),
    // files: Joi.object()
    // })
  }),
  errors(),
  (request, response) => {
    signUpController.signUp(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

signUpRouter.put("/", (request, response) => {
  // put method
});

signUpRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = signUpRouter;
