const orgModule2Router = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const orgModule2Controller = require("../controllers").orgModule2;

orgModule2Router.get("/getAllOrgModules", (request, response) => {
  orgModule2Controller.getAllOrgModule(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

orgModule2Router.get(
  "/getOrgModule",
  (request, response) => {
    orgModule2Controller.getOrgModule(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModule2Router.post(
  "/createOrgModule",
  (request, response) => {
    orgModule2Controller.insertOrgModule(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModule2Router.put(
  "/editOrgModule",

  (request, response) => {
    orgModule2Controller.editOrgModule(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModule2Router.delete(
  "/deleteOrgModule",

  (request, response) => {
    orgModule2Controller.deleteOrgModule(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);


module.exports = orgModule2Router;