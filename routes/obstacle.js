const obstacleRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const obstacleController = require("../controllers").obstacle;

const { celebrate, Joi, errors } = require("celebrate");

obstacleRouter.get("/getAllObstacles", (request, response) => {
  obstacleController.getAllObstacle(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

obstacleRouter.get(
  "/getObstacle",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    obstacleController.getObstacle(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

obstacleRouter.post(
  "/createObstacle",
  celebrate({
    body: Joi.object().keys({
      fields: Joi.object().keys({
        name: Joi.string().required(),
        number: Joi.number().negative().required(),
      }),
      files: Joi.object().keys({
        image: Joi.object(),
      }),
    }),
  }),
  errors(),
  (request, response) => {
    obstacleController.insertObstacle(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

obstacleRouter.put(
  "/editObstacle",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      fields: Joi.object().keys({
        name: Joi.string(),
        number: Joi.number(),
      }),
      files: Joi.object().keys({
        image: Joi.object(),
      }),
    }),
  }),
  errors(),
  (request, response) => {
    obstacleController
      .editObstacle({ ...request.body, _id: request.query.id })
      .then(
        (res) => response.status(res.status).json(res),
        (err) => response.status(err.status).json(err)
      );
  }
);

obstacleRouter.delete(
  "/deleteObstacle",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    obstacleController.deleteObstacle(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = obstacleRouter;
