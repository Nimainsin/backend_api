const chatRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const chatController = require("../controllers").chat;
const { celebrate, Joi, errors } = require("celebrate");

chatRouter.get("/getAllChats",
  celebrate({
    query: Joi.object().keys({
      contestId: Joi.string().required(),
      groupId: Joi.string().required(),
    })
  }), errors()
  , (request, response) => {
    chatController.getAllChats(request.query).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  });

chatRouter.get(
  "/getChat/:id",
  celebrate({
    params: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    chatController.getChat(request.params.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

chatRouter.post(
  "/createChat",
  celebrate({
    body: Joi.object().keys({
      contestId: Joi.string().required(),
      userId: Joi.string().required(),
      message:Joi.string().required()
    }),
  }),
  errors(),
  (request, response) => {
    chatController.insertChat(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);






chatRouter.put(
  "/editChat/:id",
  celebrate({
    params: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      message: Joi.string().required()
    }),
  }),
  errors(),
  (request, response) => {
    chatController.editChat(request.body, request.params.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

chatRouter.delete(
  "/deleteChat/:id",
  celebrate({
    params: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    chatController.deleteChat(request.params.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = chatRouter;