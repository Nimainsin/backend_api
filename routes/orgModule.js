const orgModuleRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const orgModuleController = require("../controllers").orgModule;
const { celebrate, Joi, errors } = require("celebrate");

orgModuleRouter.get("/getAllOrgModules", (request, response) => {
  orgModuleController.getAllOrgModule(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

orgModuleRouter.get(
  "/getOrgModule",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgModuleController.getOrgModule(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModuleRouter.post(
  "/createOrgModule",
  celebrate({
    body: Joi.object().keys({
      moduleName: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    orgModuleController.insertOrgModule(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModuleRouter.put(
  "/editOrgModule",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      moduleName: Joi.string(),
    }),
  }),
  errors(),
  (request, response) => {
    orgModuleController.editOrgModule(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

orgModuleRouter.delete(
  "/deleteOrgModule",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    orgModuleController.deleteOrgModule(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = orgModuleRouter;
