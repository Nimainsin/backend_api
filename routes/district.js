const districtRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const districtController = require("../controllers").district;
const { celebrate, Joi, errors } = require("celebrate");

districtRouter.get("/getAllDistricts", (request, response) => {
  districtController.getAllDistrict(request.query).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

districtRouter.get("/getAllDistrictsByState/:id", celebrate({
  params: Joi.object().keys({
    id: Joi.string().required()
  })
}), errors(), (request, response) => {
  districtController.getAllDistrictsByState({ ...request.query, stateId: request.params.id }).then(
    (res) => response.status(res.status).json(res),
    (err) => response.status(err.status).json(err)
  );
});

districtRouter.get(
  "/getDistrict",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    districtController.getDistrict(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

districtRouter.post(
  "/createDistrict",
  celebrate({
    body: Joi.object().keys({

      name: Joi.string().required(),
      stateId: Joi.string().required(),
    }),
  }),
  errors(),
  (request, response) => {
    districtController.insertDistrict(request.body).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

districtRouter.put(
  "/editDistrict",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
    body: Joi.object().keys({
      //  fields: Joi.object().keys({
      name: Joi.string(),
      stateId: Joi.string(),
      //   }),
      //   files: Joi.object(),
    }),
  }),
  errors(),
  (request, response) => {
    districtController.editDistrict(request.body, request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

districtRouter.delete(
  "/deleteDistrict",
  celebrate({
    query: {
      id: Joi.string().required(),
    },
  }),
  errors(),
  (request, response) => {
    districtController.deleteDistrict(request.query.id).then(
      (res) => response.status(res.status).json(res),
      (err) => response.status(err.status).json(err)
    );
  }
);

module.exports = districtRouter;
