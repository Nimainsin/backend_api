const roleRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const roleController = require("../controllers").role;

roleRouter.get("/", (request, response) => {
  // get method
});

roleRouter.post("/", (request, response) => {
  // post method
});

roleRouter.put("/", (request, response) => {
  // put method
});

roleRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = roleRouter;