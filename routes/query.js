const queryRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const queryController = require("../controllers").query;

queryRouter.get("/", (request, response) => {
  // get method
});

queryRouter.post("/insertQuery", (request, response) => {
  queryController.insertQuery({ queryObj: request.body, captcha: request.body.captcha, remoteAddress: request.connection.remoteAddress })
    .then(res =>
      response.status(res.status).json(res)
      ,
      err => response.status(err.status).json(err))
});

queryRouter.put("/", (request, response) => {
  // put method
});

queryRouter.delete("/", (request, response) => {
  // delete method
});

module.exports = queryRouter;