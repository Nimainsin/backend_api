const videoRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const videoController = require("../controllers").video;
const { celebrate, Joi, errors } = require("celebrate");

videoRouter.get("/getAllVideo", (request, response) => {
    videoController.getAllVideo(request.query).then(
        (res) => response.status(res.status).json(res),
        (err) => response.status(err.status).json(err)
    );
});

videoRouter.get(
    "/getVideo",
    celebrate({
        query: {
            id: Joi.string().required(),
        },
    }),
    errors(),
    (request, response) => {
        videoController.getVideo(request.query.id).then(
            (res) => response.status(res.status).json(res),
            (err) => response.status(err.status).json(err)
        );
    }
);

videoRouter.post(
    "/createVideo",
    celebrate({
        body: Joi.object().keys({
            fields: Joi.object().keys({
                name: Joi.string(),
                description: Joi.string(),
                fileType: Joi.string().valid(
                    "webm",
                    "flv",
                    "wmv",
                    "amv",
                    "mp4",
                    "m4p",
                    "3gp",
                    "gif"
                ),
                previewImage: Joi.string(),
                videoLengthTime: Joi.string(),
                videoType: Joi.string().valid("portrait", "landscape"),
            }),
            files: Joi.object()
        })
    }),
    errors(),
    (request, response) => {
        console.log("request.body", request.body);
        videoController.insertVideo(request.body).then(
            (res) => response.status(res.status).json(res),
            (err) => response.status(err.status).json(err)
        );
    }
);

videoRouter.put(
    "/editVideo",
    celebrate({
        query: {
            id: Joi.string().required(),
        },
        body: Joi.object().keys({
            fields: Joi.object().keys({
                name: Joi.string(),
                description: Joi.string(),
                fileType: Joi.string().valid(
                    "webm",
                    "flv",
                    "wmv",
                    "amv",
                    "mp4",
                    "m4p",
                    "3gp",
                    "gif"
                ),
                previewImage: Joi.string(),
                videoLengthTime: Joi.string(),
                videoType: Joi.string().valid("portrait", "landscape"),
            }),
            files: Joi.object()
        })
    }),
    errors(),
    (request, response) => {
        videoController.editVideo({ ...request.body, id: request.query.id }).then(
            (res) => response.status(res.status).json(res),
            (err) => response.status(err.status).json(err)
        );
    }
);

videoRouter.delete(
    "/deleteVideo",
    celebrate({
        query: {
            id: Joi.string().required(),
        },
    }),
    errors(),
    (request, response) => {
        videoController.deleteVideo(request.query.id).then(
            (res) => response.status(res.status).json(res),
            (err) => response.status(err.status).json(err)
        );
    }
);

module.exports = videoRouter;