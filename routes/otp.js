const otpRouter = require("kvell-scripts").router();
// eslint-disable-next-line no-unused-vars
const otpController = require("../controllers").otp
const { Joi, celebrate, errors } = require('celebrate')
otpRouter.post("/sendOtp", celebrate({
  body: Joi.object().keys({
    mobile: Joi.number().required()
  })
}), errors(), (request, response) => {
  otpController.createOtp(request.body).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

otpRouter.post("/verifyOtp", celebrate({
  body: Joi.object().keys({
    mobile: Joi.number().required(),
    otp: Joi.number().required(),
  })
}), errors(), (request, response) => {
  otpController.verifyOtp(request.body).then(res => response.status(res.status).json(res),
    err => response.status(err.status).json(err))
});

module.exports = otpRouter;